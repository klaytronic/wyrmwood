﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GroundTexture : MonoBehaviour
{
    float timer;
    public float moveTime;

    public AnimationCurve animationCurve;
    public GameObject groundTexture;
    Image groundTextureImage;
    public float evaluation;

    // Start is called before the first frame update
    void Start()
    {
        timer = 0;
        groundTextureImage = groundTexture.GetComponent<Image>();
    }


    // Update is called once per frame
    private void Update()
    {
        if (timer < 1 )
        {
           evaluation = Mathf.Lerp(0f, 1f, animationCurve.Evaluate(timer / moveTime));
            groundTextureImage.fillAmount = evaluation;

            timer += Time.deltaTime;
        }
       
    }
}
