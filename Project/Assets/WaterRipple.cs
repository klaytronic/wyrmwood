﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterRipple : MonoBehaviour
{
    public ParticleSystem waterRippleParticleSystem;

    // Start is called before the first frame update
    private void Awake()
    {
        waterRippleParticleSystem = GetComponentInChildren<ParticleSystem>();
    }

    public void CreateWaterRipple()
    {
		if (waterRippleParticleSystem)
			waterRippleParticleSystem.Play();
    }
}
