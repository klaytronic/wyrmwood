﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTimer : MonoBehaviour
{
	[SerializeField] float duration;
    public GameObject trailParticleSystem;
    private void Start()
    {
        StartCoroutine(DestroyGameobject(duration));
    }

    private IEnumerator DestroyGameobject(float duration)
    {
        yield return new WaitForSeconds(duration);
        if (trailParticleSystem)
        {
            trailParticleSystem.transform.SetParent(null);
            ParticleSystem m_particleSystem = trailParticleSystem.GetComponent<ParticleSystem>();
            m_particleSystem.Stop();
        }
        Destroy(gameObject);
    }
}
