﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	public bool IsPersistent = true;

	private static T sm_instance;

	public static bool HasInstance
	{
		get
		{
			return sm_instance != null;
		}
	}

	public static T Instance
	{
		get
		{
			return sm_instance;
		}
	}

	private static void SetInstance( T component )
	{
		sm_instance = component;
		if ( null == sm_instance )
		{
			Debug.LogError( "Somehow the singleton can't get a reference to itself. Ask a developer to debug." );
		}
	}

	public virtual void Awake()
	{
		if ( HasInstance )
		{
			if ( Instance != this )
			{
				Debug.Log( "Preventing creation of a second Singleton of type " + typeof( T ).ToString() + ". Destroying game object." );
				Destroy( this.gameObject );
				return;
			}
		}

		Debug.Log( "Singleton of type " + typeof( T ).ToString() + " is being created" );
		SetInstance( GetComponent<T>() );
		if ( IsPersistent )
		{
			DontDestroyOnLoad( this.gameObject );
		}
	}

	protected void ClearInstance()
	{
		sm_instance = null;
	}
}

