﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class CardProperties : MonoBehaviour
{
	TMP_Text descText;
	Image art, frame;

	[Header("General Properties")]
	public bool playable;
	public CardInfo info;

	List<CardEffect> effects;

	int effectNum, completeNum;
	[System.NonSerialized] public bool completed;

	private void Awake()
	{
		// get needed components
		art = transform.GetChild(0).GetComponent<Image>();
		effects = new List<CardEffect>();
		effects.AddRange(GetComponents<CardEffect>());

		// if awake is being called, this card is being drawn. activate the draw condition
		ActivateCondition(Condition.Draw);
	}

	// set up the display.
	private void Start()
	{
		art.sprite = info.fullImage;
	}

	public void SetPlayable(bool toggle)
	{
		playable = toggle;

		if (playable)
			art.color = Color.white;
		else
			art.color = Color.red;
	}

	// this coroutine is called when a card's activation condition is met. The specific condition to check for effects is sent as an enum.
	public IEnumerator ActivateCondition(Condition active)
	{
		completed = false;
		effectNum = 0;

		// activate all effects in order, yielding on effects that are set to wait for their completion
		foreach (CardEffect effect in effects)
		{
			if (effect.condition == active)
			{
				effectNum++;
				if (effect.waitToFinish)
				{
					StartCoroutine(effect.Invoke());
					while (!effect.completed)
						yield return null;
				}					
				else
					StartCoroutine(effect.Invoke());
			}
		}

		// wait until all effects are complete to say this card is complete
		while (effectNum != completeNum)
		{
			completeNum = 0;
			foreach (CardEffect effect in effects)
			{
				if (effect.condition == active)
				{
					if (!effect.completed)
						break;
					else
						completeNum++;						
				}
			}
			yield return null;
		}
		completed = true;
	}
}
