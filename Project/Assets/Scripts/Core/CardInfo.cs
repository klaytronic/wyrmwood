﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "New Card Info", menuName = "Card Info", order = 1)]
public class CardInfo : ScriptableObject
{
	public string title = "Placeholder Supreme";
	[MultiLineProperty(5)] public string description = "lorem ipsum and and all that jazz.";
    [MultiLineProperty(5)] public string tipDescription = "lorem ipsum and all that jazz";
	public int manaCost = 0;
	[PreviewField(100, ObjectFieldAlignment.Right)] public Sprite thumb;
	[PreviewField(100, ObjectFieldAlignment.Right)] public Sprite fullImage;
	public GameObject reticle;
	public bool cursorAim;
}
