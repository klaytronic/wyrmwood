﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitstunManager : MonoBehaviour
{
	public float stunLength;

	public Vector3 playerHitShakeIntensity;
	public float playerHitShakeLength;

	public Vector3 enemyHitShakeIntensity;
	public float enemyHitShakeLength;

	SpriteRenderer player;

	private void Start()
	{
		player = PlayerController.Instance.GetComponentInChildren<SpriteRenderer>();
	}

	// called when the player is hit.
	public void HitPlayer(float damage)
	{
		StartCoroutine(PlayerHitSequence());
	}

	// called when the enemy is hit.
	public void HitEnemy(GameObject enemy, float damage, bool stunPlayer)
	{
		StartCoroutine(EnemyHitSequence(enemy, damage, stunPlayer));
	}

	IEnumerator EnemyHitSequence(GameObject enemy, float damage, bool stunPlayer)
	{

		if (stunPlayer)
		{
			PlayerController.Instance.ToggleInput(false);
			ServiceLocator.Instance.cardSystem.ToggleInput(false);
			PlayerController.Instance.anim.speed = 0;
			iTween.ShakePosition(Camera.main.gameObject, enemyHitShakeIntensity, enemyHitShakeLength);
		}

		float timer = 0;
		while (timer < stunLength)
		{
			timer += Time.deltaTime;
			yield return null;
		}

		if (stunPlayer)
		{
			PlayerController.Instance.ToggleInput(true);
			ServiceLocator.Instance.cardSystem.ToggleInput(true);
			PlayerController.Instance.anim.speed = 1;
		}
	}

	IEnumerator PlayerHitSequence()
	{
		Time.timeScale = 0;
		player.color = Color.red;


		float timer = 0;
		while (timer < stunLength)
		{
			timer += Time.unscaledDeltaTime;
			yield return null;
		}

		iTween.ShakePosition(Camera.main.gameObject, playerHitShakeIntensity, playerHitShakeLength);

		player.color = Color.white;
		Time.timeScale = 1;
	}
}
