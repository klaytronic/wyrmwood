﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;

public class ServiceLocator : Singleton<ServiceLocator>
{
	[HideInInspector] public CardSystem cardSystem;
	[HideInInspector] public GameObject cardAquisitionUI;
	CameraControl cam;
	[HideInInspector] public DeckAcquire deck;
	[HideInInspector] public HitstunManager hitstun;

    [HideInInspector] public LevelManager levelManager;

	[HideInInspector] public bool deckGot = false;
	[HideInInspector] public Image fade;
	public float fadeLength;

	[PropertyRange(0,100)] public float manaDropChance;
	public GameObject manaPickup;

    public CanvasGroup buttonPromptUI;
    GameObject buttonCircle;
    GameObject trigger;
    GameObject stick;
    GameObject bumpers;

    public float promptFadeDuration;

	public override void Awake()
	{
        base.Awake();
		cardSystem = GetComponent<CardSystem>();
		hitstun = GetComponent<HitstunManager>();
        levelManager = GetComponent<LevelManager>();
    }

	public void Start()
	{
		fade = GameObject.Find("Fade").GetComponent<Image>();
		cardAquisitionUI = GameObject.Find("CardAcquireUI").transform.GetChild(1).gameObject;
        cardAquisitionUI.transform.parent.GetChild(0).gameObject.SetActive(false);
        SceneManager.sceneLoaded += LevelInitialize;
		LevelInitialize(SceneManager.GetActiveScene(), LoadSceneMode.Single);

        buttonCircle = buttonPromptUI.transform.GetChild(0).gameObject;
        trigger = buttonPromptUI.transform.GetChild(1).gameObject;
        stick = buttonPromptUI.transform.GetChild(2).gameObject;
        bumpers = buttonPromptUI.transform.GetChild(3).gameObject;

        buttonPromptUI.alpha = 0;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= LevelInitialize;
    }

    private void LevelInitialize(Scene scene, LoadSceneMode mode)
	{
		cam = Camera.main.GetComponent<CameraControl>();
        cam.target = PlayerController.Instance.transform;

		GameObject temp = GameObject.Find("DeckAcquire");

		if (temp)
			deck = temp.GetComponent<DeckAcquire>();

		if (deck && deckGot)
			deck.gameObject.SetActive(false);

		PlayerController.Instance.NewScene();
		PlayerController.Instance.health.NewScene();
		cardSystem.NewScene();

        ButtonUnprompt();
	}

    public void ButtonPrompt(InputButton prompt)
    {
        iTween.StopByName("FadeOut");
        switch (prompt)
        {
            case InputButton.Dash:
                buttonCircle.SetActive(true);
                trigger.SetActive(false);
                stick.SetActive(false);
                bumpers.SetActive(false);
                buttonCircle.GetComponentInChildren<TMP_Text>().text = "A";
                break;
            case InputButton.Mulligan:
                buttonCircle.SetActive(true);
                trigger.SetActive(false);
                stick.SetActive(false);
                bumpers.SetActive(false);
                buttonCircle.GetComponentInChildren<TMP_Text>().text = "Y";
                break;
            case InputButton.Cast:
                trigger.SetActive(true);
                buttonCircle.SetActive(false);
                stick.SetActive(false);
                bumpers.SetActive(false);
                break;
            case InputButton.Select:
                bumpers.SetActive(true);
                buttonCircle.SetActive(false);
                trigger.SetActive(false);
                stick.SetActive(false);
                break;
            case InputButton.Move:
                stick.SetActive(true);
                buttonCircle.SetActive(false);
                trigger.SetActive(false);
                bumpers.SetActive(false);
                stick.GetComponentInChildren<TMP_Text>().text = "L";
                break;
            case InputButton.Aim:
                stick.SetActive(true);
                buttonCircle.SetActive(false);
                trigger.SetActive(false);
                bumpers.SetActive(false);
                stick.GetComponentInChildren<TMP_Text>().text = "R";
                break;
            case InputButton.Unique:
                stick.SetActive(false);
                buttonCircle.SetActive(false);
                trigger.SetActive(false);
                bumpers.SetActive(false);
                break;
        }

        iTween.ValueTo(this.gameObject, iTween.Hash("from", buttonPromptUI.alpha,
                            "to", 1,
                            "time", promptFadeDuration,
                            "onupdate", "UpdatePromptFade",
                            "onupdatetarget", this.gameObject));
    }

    public void ButtonUnprompt()
    {
        iTween.ValueTo(this.gameObject, iTween.Hash("from", buttonPromptUI.alpha,
                    "to", 0,
                    "time", promptFadeDuration,
                    "onupdate", "UpdatePromptFade",
                    "oncomplete", "FinishPromptHide",
                    "name", "FadeOut"));
    }

    public void UpdatePromptFade(float alpha)
    {
        buttonPromptUI.alpha = alpha;
    }

    public void FinishPromptHide()
    {
        buttonPromptUI.alpha = 0;
        buttonCircle.SetActive(false);
        trigger.SetActive(false);
        stick.SetActive(false);
        bumpers.SetActive(false);
    }

    void FadeSequence(float alpha)
	{
		fade.color = new Color(fade.color.r, fade.color.g, fade.color.b, alpha);
	}

	void FinishFade()
	{
		fade.color = Color.clear;
		fade.enabled = true;
		PlayerController.Instance.ToggleInput(true);
		ServiceLocator.Instance.cardSystem.ToggleInput(true);
	}

	void FinishFadeWhite()
	{
		fade.color = Color.white;
		fade.enabled = true;
	}

	public void FadeWhite()
	{
		fade.enabled = true;
		fade.color = new Color(1, 1, 1, 0);
		iTween.ValueTo(this.gameObject, iTween.Hash("from", 0,
													"to", 1,
													"time", fadeLength,
													"onupdate", "FadeSequence",
													"onupdatetarget", this.gameObject,
													"oncomplete", "FinishFadeWhite",
													"oncompletetarget", this.gameObject));
	}

    public void FadeClear()
    {
        fade.enabled = true;
        iTween.ValueTo(this.gameObject, iTween.Hash("from", fade.color.a,
                                                    "to", 0,
                                                    "time", fadeLength,
                                                    "onupdate", "FadeSequence",
                                                    "onupdatetarget", this.gameObject,
                                                    "oncomplete", "FinishFade",
                                                    "oncompletetarget", this.gameObject));
    }
}
