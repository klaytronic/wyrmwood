﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public enum Condition {Play, Draw, Discard, Banish, Special}

public abstract class CardEffect : MonoBehaviour
{
	public Condition condition;

	public bool waitToFinish;

	[HideInInspector] public bool completed;

	[Header("Timing")]
	public float startLag;
	public float endLag;

	public virtual IEnumerator Invoke()
	{
		yield break;
	}
}

public abstract class CardEffectData
{
	public Condition activationCondition;
	[HideInInspector] public bool waitToFinish = false;
	string waitName = "Let Next Effect Play Immediately";
	[PropertyTooltip("Should this effect make later effects wait for this one to finish?")]
	[Button("$waitName", ButtonSizes.Small), GUIColor("WaitColor")]
	[PropertyOrder(-1)]
	[ButtonGroup("Other")]
	void ToggleWait()
	{
		waitToFinish = !waitToFinish;
		//waitName = waitToFinish ? "Make Next Effect Wait for Finish" : "Let Next Effect Play Immediately";
	}
	Color WaitColor()
	{
		waitName = waitToFinish ? "Make Next Effect Wait for Finish" : "Let Next Effect Play Immediately";
		return waitToFinish ? Color.red : Color.green;
	}

	[Header("Timing")]
	public float startLag;
	public float endLag;
}
