﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cutscene : MonoBehaviour
{
    protected CameraControl cam;
    protected Animator anim;
    protected int actionIndex = 0;
    public string animationName;

    public CameraAction[] cutsceneSequence;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main.GetComponent<CameraControl>();
        anim = GetComponent<Animator>();
    }

    protected virtual void StartCutscene()
    {
        if (string.IsNullOrWhiteSpace(animationName))
        {
            Debug.Log("This cutscene specifies no animation! nothing will play!");
            return;
        }

        PlayerController.Instance.ToggleInput(false);
        ServiceLocator.Instance.cardSystem.ToggleInput(false);

        anim.Play(animationName);
    }

    public virtual void NextCameraAction()
    {
        if (actionIndex >= cutsceneSequence.Length)
        {
            Debug.Log(this.name + ": The cutscene animator has more action calls than actions!");
            return;
        }

        CameraAction currentAction = cutsceneSequence[actionIndex];

        cam.trackRate = currentAction.moveSpeed > 0 ? currentAction.moveSpeed : cam.trackRate;

        if (currentAction.returnToPlayer)
            cam.ReturnToPlayer();
        else if (currentAction.target != null)
            cam.target = currentAction.target;

        if (!string.IsNullOrWhiteSpace(currentAction.customMethodName))
            StartCoroutine(currentAction.customMethodName);

        actionIndex++;
    }
}
