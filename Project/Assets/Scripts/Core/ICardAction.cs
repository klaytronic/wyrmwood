﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICardAction
{
	void Play();

	void Discard();

	void Draw();

	void Banish();
}
