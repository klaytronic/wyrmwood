﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEditor;

[GlobalConfig("Assets/Scripts/Core/")]
public class CardDB : GlobalConfig<CardDB>
{
	public List<GameObject> cards;

	[PreviewField(100, ObjectFieldAlignment.Right)] public Sprite[] mana;
}
