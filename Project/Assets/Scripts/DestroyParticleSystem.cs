﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParticleSystem : MonoBehaviour
{
    ParticleSystem m_particleSystem;

    // Start is called before the first frame update
    void Start()
    {
        m_particleSystem = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (m_particleSystem.particleCount <=0)
        {
            Destroy(this.gameObject);
        }
    }
}
