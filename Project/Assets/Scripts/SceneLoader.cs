﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
	[SerializeField] int sceneIndex;
	Image fade;

	public void InitializeLoad()
	{
		PlayerController.Instance.ToggleInput(false);
		ServiceLocator.Instance.cardSystem.ToggleInput(false);
		fade = ServiceLocator.Instance.fade;

		fade.enabled = true;
		fade.color = Color.clear;
		iTween.ValueTo(this.gameObject, iTween.Hash("from", 0,
													"to", 1,
													"time", ServiceLocator.Instance.fadeLength,
													"onupdate", "FadeSequence",
													"onupdatetarget", this.gameObject,
													"oncomplete", "LoadSceneAfter",
													"oncompletetarget", this.gameObject));
	}

	public void FadeSequence(float alpha)
	{
		fade.color = new Color(0, 0, 0, alpha);
	}

	public void LoadSceneAfter()
	{
		fade.color = Color.black;
        StartCoroutine(LoadSequence());
	}

    IEnumerator LoadSequence()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);

            yield return null;
        }
    }
}
