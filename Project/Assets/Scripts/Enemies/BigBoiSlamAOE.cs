﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBoiSlamAOE : MonoBehaviour
{
    private GameObject player;
    [SerializeField] private GameObject owner;
    [SerializeField] float speed = 10;
    [SerializeField] string collisionTag;
    [SerializeField] int damage = 5;

    public CapsuleCollider2D AOECollider;
    private bool canDamage = true;
    public bool moving;
    private Animator animator;
    private EnemyHealth bigboyHealth;

    // Start is called before the first frame update
    void Start()
    {
        bigboyHealth = GetComponentInParent<EnemyHealth>();
        bigboyHealth.DeathEvent.AddListener(DestroyThis);
        animator = GetComponentInChildren<Animator>();
        player = PlayerController.Instance.gameObject;
        AOECollider = GetComponentInChildren<CapsuleCollider2D>();
        transform.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (moving)
        {
            MoveToPlayer();
        }
    }

    void MoveToPlayer()
    {
        transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
    }

    public void EnableDamage()
    {
        AOECollider.enabled = true;
    }

    public void Reset()
    {
        AOECollider.enabled = false;
        canDamage = true;
        gameObject.SetActive(false);

    }

    public void EnableAttack()
    {
        transform.position = owner.transform.position;
        gameObject.SetActive(true);
        moving = true;
        animator.SetBool("Moving", moving);
        animator.Play("Flash");
    }

    public void StopMoving()
    {
        moving = false;
        animator.SetBool("Moving", moving);
    }

    private void DestroyThis()
    {
        GameObject.Destroy(gameObject);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (canDamage)
        {
            if (collision.gameObject.CompareTag(collisionTag))
            {
                if (collision.GetComponentInParent<HealthBase>())
                {
                    collision.GetComponentInParent<HealthBase>().ReceiveDamage(damage);
                    Debug.Log("Hi");
                    canDamage = false;
                }
            }
        }
    }
}
