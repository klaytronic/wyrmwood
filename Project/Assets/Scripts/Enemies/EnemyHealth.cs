﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMOD.Studio;

public class EnemyHealth : HealthBase
{
    [SerializeField]
    private HealthBarController healthBar;
    [SerializeField]
    private GameObject refTextObj;
    [SerializeField]
    private Transform damageSpawnLocation;
    private GameObject thisTextObj;
    [SerializeField]
    private GameObject deathParticle;

    [FMODUnity.EventRef]
    public string death = "event:/EnemyDeath";
    [FMODUnity.EventRef]
    public string takehit = "event:/EnemyTakeDamage";

    public FMOD.Studio.EventInstance hitEv;
    public FMOD.Studio.EventInstance deathEv;

    private void Start()
    {
        if (healthBar == null)
        {
            healthBar = GetComponentInChildren<HealthBarController>();
            if (healthBar == null)
            {
                Debug.LogError("There is no HealthBarController script attached to " + gameObject.name + ", please add a HealthBarController to a health bar canvas item.");
            }
        }

        TookDamageEvent.AddListener(HealthChange);
        DeathEvent.AddListener(SpawnDeathParticle);
        healthBar.updateHP((int)currentHealth);
        healthBar.healthSlider.maxValue = currentHealth;
        healthBar.gameObject.SetActive(false);

        hitEv = FMODUnity.RuntimeManager.CreateInstance(takehit);
        deathEv = FMODUnity.RuntimeManager.CreateInstance(death);
    }

    private void SpawnDeathParticle()
    {
        deathEv.start();

        Instantiate(deathParticle, transform.position, Quaternion.identity, null);
    }

    public void HealthChange(float amount)
    {
        //Turns health bar on if it wasn't visible before
        if (!healthBar.gameObject.activeSelf)
        {
            healthBar.gameObject.SetActive(true);
        }

        //Create text for healthbar
        thisTextObj = Instantiate(refTextObj, damageSpawnLocation.position, Quaternion.identity, null);
        thisTextObj.GetComponent<TextMesh>().text = amount.ToString();

        healthBar.updateHP((int)currentHealth);

        hitEv.start();
    }
}
