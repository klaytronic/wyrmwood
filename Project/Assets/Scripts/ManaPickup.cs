﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaPickup : MonoBehaviour
{
	bool activated = false;
	[SerializeField] float moveForce = 100;
	[SerializeField] float collectDist = 0.2f;
	Rigidbody2D rb;

    [FMODUnity.EventRef]
    public string PU = "event:/Pickup";

    public FMOD.Studio.EventInstance pickupEv;
   
    // Start is called before the first frame update
    void Start()
    {
        pickupEv = FMODUnity.RuntimeManager.CreateInstance(PU);
        rb = GetComponent<Rigidbody2D>();
    }

	private void OnTriggerStay2D(Collider2D collision)
	{
		if (collision.tag == "Player" && ServiceLocator.Instance.cardSystem.mana < ServiceLocator.Instance.cardSystem.maxMana && !activated)
		{
			activated = true;
            pickupEv.start();
			StartCoroutine(MoveSequence());
		}
	}

	IEnumerator MoveSequence()
	{
		while (true)
		{
			if (ServiceLocator.Instance.cardSystem.mana == ServiceLocator.Instance.cardSystem.maxMana)
			{
				activated = false;
				yield break;
			}

			rb.AddForce((PlayerController.Instance.transform.position - transform.position).normalized * moveForce);

			if ((PlayerController.Instance.transform.position - transform.position).magnitude <= collectDist)
			{
				ServiceLocator.Instance.cardSystem.gainMana(1);
				Destroy(this.gameObject);
				yield break;
			}

			yield return null;
		}
	}
}
