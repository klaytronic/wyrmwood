﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.PostProcessing;

public class DeckAcquire : Cutscene
{
	CardAquisition[] phases;

	public int acqIndex;
	public float effectRadius = 5.5f;
	public float minPlayerSpeed = 0.5f;
	public float timeSpacing = 0.5f;
	public Vector3 shakeAmount = Vector3.one;

    public UnityEvent StopPrompts;
    public UnityEvent FinalPrompt;

    CanvasGroup bumpers;

	bool shaking = false;
	public Transform altar;
	Transform playerPos;
	PostProcessVolume vol;
	bool rigged = false;

    public ParticleSystem alterParticleSystem;
    ParticleSystem.VelocityOverLifetimeModule velocityModule;
    ParticleSystem.NoiseModule noiseModule;

    public GameObject cardPickup;
    // Start is called before the first frame update
    void Start()
    {
		phases = GetComponentsInChildren<CardAquisition>();
		cam = Camera.main.GetComponent<CameraControl>();
		vol = GetComponentInChildren<PostProcessVolume>();
		playerPos = transform.GetChild(1);
		anim = GetComponent<Animator>();

        velocityModule = alterParticleSystem.velocityOverLifetime;
        noiseModule = alterParticleSystem.noise;
    }

	private void Update()
	{
		if (!rigged)
		{
			if ((transform.position - PlayerController.Instance.transform.position).magnitude < effectRadius)
			{
				//PlayerController.Instance.currentSpeed = Mathf.Lerp(minPlayerSpeed, PlayerController.Instance.maxSpeed, (transform.position - PlayerController.Instance.transform.position).magnitude / effectRadius);
				vol.weight = Mathf.Lerp(1, 0, (transform.position - PlayerController.Instance.transform.position).magnitude / effectRadius);
				cam.target = altar;

				if (!shaking)
				{
					shaking = true;
					iTween.ShakePosition(Camera.main.transform.parent.gameObject, iTween.Hash("amount", shakeAmount * Mathf.Lerp(0, 1, (effectRadius / (transform.position - PlayerController.Instance.transform.position).magnitude) / effectRadius),
																			 "oncomplete", "ShakeEnd",
																			 "oncompletetarget", this.gameObject,
																			 "time", 1f,
																			 "looptype", LoopType.pingPong));
				}
			}
			else
			{
				//PlayerController.Instance.currentSpeed = PlayerController.Instance.maxSpeed;
				cam.target = PlayerController.Instance.transform;
				vol.weight = 0;
			}
		}			
	}

	void ShakeEnd()
	{
		shaking = false;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
			StartCutscene();			
		}
	}

	IEnumerator TriggerDraft()
	{
        bumpers = ServiceLocator.Instance.cardAquisitionUI.transform.parent.GetChild(0).GetComponent<CanvasGroup>();
        bumpers.gameObject.SetActive(true);
        float duration = phases[0].fadeDuration;

        vol.enabled = false;

        iTween.ValueTo(this.gameObject, iTween.Hash("from", 0,
                                            "to", 1,
                                            "time", duration,
                                            "onupdatetarget", this.gameObject,
                                            "onupdate", "UpdateBumper"));

        for (int index = 0; index < phases.Length; ++index)
		{
			StartCoroutine(phases[index].StartAcquisition());

			while (!phases[index].completed)
				yield return 0;

			float timer = 0;
			while (timer < timeSpacing)
			{
				timer += Time.deltaTime;
				yield return null;
			}
		}

        iTween.ValueTo(this.gameObject, iTween.Hash("from", 1,
                                    "to", 0,
                                    "time", duration,
                                    "onupdatetarget", this.gameObject,
                                    "onupdate", "UpdateBumper",
                                    "oncomplete", "FinishBumper"));

        ServiceLocator.Instance.FadeClear();
        ServiceLocator.Instance.fade.GetComponentInChildren<WaveText>().FadeOutText();

        GetComponentInChildren<Collider2D>().enabled = false;
        cam.target = PlayerController.Instance.transform;

		ServiceLocator.Instance.deckGot = true;

        FinalPrompt.Invoke();

		yield break;
	}

    public void UpdateBumper(float alpha)
    {
        bumpers.alpha = alpha;
    }

    public void FinishBumper()
    {
        bumpers.alpha = 0;
        bumpers.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }

	IEnumerator RigMovement()
	{
		rigged = true;
		//PlayerController.Instance.currentSpeed = PlayerController.Instance.maxSpeed;
		PlayerController.Instance.StartRiggedMovement(playerPos.position, Vector3.up);

        StopPrompts.Invoke();
		yield break;
	}

	IEnumerator Shake()
	{
		iTween.ShakePosition(Camera.main.transform.parent.gameObject, iTween.Hash("amount", shakeAmount,
														 "oncomplete", "ShakeEnd",
														 "oncompletetarget", this.gameObject,
														 "time", 5f));
		yield break;
	}

	IEnumerator FadeToWhite()
	{
		ServiceLocator.Instance.FadeWhite();
		yield break;
	}

	IEnumerator FadeInText()
	{
		ServiceLocator.Instance.fade.GetComponentInChildren<WaveText>().FadeInText();
		yield break;
	}
    IEnumerator TurnOffAlter()
    {
        alterParticleSystem.enableEmission = false;
        velocityModule.speedModifier = 0.5f;
        noiseModule.strength = 1f;

        Destroy(cardPickup);
        yield break;
    }
}
