﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test_aquire : MonoBehaviour
{
	CardAquisition run;
	private void Start()
	{
		run = GetComponent<CardAquisition>();	
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
			StartCoroutine(Trigger());
		}
	}

	IEnumerator Trigger()
	{
		StartCoroutine(run.StartAcquisition());

		while (!run.completed)
			yield return 0;

		Destroy(this.gameObject);

		yield break;
	}
}
