﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnContact : MonoBehaviour
{
    [SerializeField]
    private string collisionTag;
    [SerializeField]
    private bool canHitMultipleTimes = false;
    [SerializeField]
    private bool hasCooldown = false;

    private float cooldownDuration = 1;
    private bool canHit = true;
    public int damage = 1;

    [FMODUnity.EventRef]
    public string hit = "event:/GhostProjectileHit";

    public FMOD.Studio.EventInstance ghosthitEv;

    // Start is called before the first frame update
    void Start()
    {
        ghosthitEv = FMODUnity.RuntimeManager.CreateInstance(hit);

        if (collisionTag == null)
        {
            Debug.LogError("collisionTag variable on object: " + transform.name + " isn't set to anything. Please fill in its tag.");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (canHit == true)
        {
            if (collision.gameObject.CompareTag(collisionTag))
            {
                ghosthitEv.start();

                if (collision.GetComponentInParent<HealthBase>())
                {
                    collision.GetComponentInParent<HealthBase>().ReceiveDamage(damage);
                    if (canHitMultipleTimes == false)
                    {
                        canHit = false;
                    }
                    if (hasCooldown)
                    {

                        StartCoroutine(Cooldown());
                    }
                }
            }
        }
        
    }

    IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(cooldownDuration);
        canHit = true;
        Debug.Log("Canhit");
    }
}
