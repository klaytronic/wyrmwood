﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class CardPickup : MonoBehaviour
{
	[ValueDropdown("GetCardList")] public int card;

    [FMODUnity.EventRef]
    public string PU = "event:/Pickup";

    public FMOD.Studio.EventInstance pickupEv;

    private void Start()
    {
        pickupEv = FMODUnity.RuntimeManager.CreateInstance(PU);
    }

    public IList<ValueDropdownItem<int>> GetCardList()
	{
		ValueDropdownList<int> list = new ValueDropdownList<int>();

		for (int i = 0; i < CardDB.Instance.cards.Count; ++i)
			list.Add(CardDB.Instance.cards[i].name, i);

		return list;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
            pickupEv.start();

            ServiceLocator.Instance.cardSystem.deckBuild.Add(card);

			ServiceLocator.Instance.cardSystem.liveDeck.Add(ServiceLocator.Instance.cardSystem.availCards[card]);

			ServiceLocator.Instance.cardSystem.RequestFillHand();

			Destroy(this.gameObject);
		}
	}
}
