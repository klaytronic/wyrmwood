﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class DamageEvent : UnityEvent<float>
{

}
public class HealEvent : UnityEvent<float>
{

}

public class HealthBase : MonoBehaviour
{
    public UnityEvent DeathEvent;
    public DamageEvent TookDamageEvent;
    public HealEvent ReceivedHealingEvent;

    protected bool dead;

	[SerializeField]
	public float maxHealth;
		
	[ReadOnly] public float currentHealth;

    // Start is called before the first frame update
    protected virtual void Awake()
    {
        //if (DeathEvent == null)
        //{
        //    DeathEvent = new UnityEvent();
        //}
        if (TookDamageEvent == null)
        {
            TookDamageEvent = new DamageEvent();
        }
        if (ReceivedHealingEvent == null)
        {
            ReceivedHealingEvent = new HealEvent();
        }

        currentHealth = maxHealth;
    }

    //private void Update()
    //{
    //    if (Input.GetMouseButtonDown(0))
    //    {
    //        ReceiveNewDot(10f, 10, 5);
    //    }
    //}

    public virtual void ReceiveDamage(float amount)
    {
        currentHealth -= amount;
        CheckIfDead();
        TookDamageEvent.Invoke(amount);
    }

    public virtual void ReceiveHealing(float amount)
    {
        //This variable is just in the event that we want healing popups
        //It makes it so if, for example, I had 8/10 hp and I received a 9 point heal, the incoming
        //heal amount would show as 2, rather than the full 9
        float healthDifference = maxHealth - currentHealth;
      
        currentHealth += amount;
        if (currentHealth > maxHealth)
        {
            ResetCurrentHealth();
        }

		ReceivedHealingEvent.Invoke(amount);
	}

    public virtual void ReceiveNewDot(float duration, int totalDamage, int totalHits)
    {
        float interval = duration / (totalHits - 1);
        float damage = totalDamage / totalHits;

        StartCoroutine(DamageOverTime(damage, interval, totalHits));
    }

    protected IEnumerator DamageOverTime(float damage, float interval, int totalhits)
    {
        float timePassed = 0;
        //float totalTime = 0;
        int count = 1;
        ReceiveDamage(damage);
        //Debug.Log("Dealt " + damage + ". Hit #: " + count + ". At " + timePassed + " interval time, and " + totalTime + " total time.");
        //Incase the final tick doesn't happen we'll need to increase total hits to run one more round
        while (count < totalhits)
        {
            timePassed += Time.deltaTime;
            //totalTime += Time.deltaTime;

            if (timePassed >= interval)
            {
                ReceiveDamage(damage);
                count += 1;
                //Debug.Log("Dealt " + damage + ". Hit #: " + count + ". At " + timePassed + " interval time, and " + totalTime + " total time.");
                timePassed = 0;
            }
            yield return null;
        }

        yield return null;
    }


    protected virtual void CheckIfDead()
    {
        if (currentHealth <= 0 && !dead)
        {
            dead = true;
            DeathEvent.Invoke();

			if (Random.value < ServiceLocator.Instance.manaDropChance / 100)
			{
				GameObject mana = Instantiate(ServiceLocator.Instance.manaPickup, transform.position, Quaternion.identity);
				mana.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * 0.5f, ForceMode2D.Impulse);
				mana.GetComponentInChildren<Animator>().Play("Burst");
			}

			GameObject.Destroy(gameObject);
        }
    }

    protected virtual void ResetCurrentHealth()
    {
        currentHealth = maxHealth;
    }
}
