﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShakeMethod : MonoBehaviour
{
    public Vector3 screenShakeIntensity;
    public float screenShakeTime;

    [FMODUnity.EventRef]
    public string gateSound = "event:/GateClose";

    public FMOD.Studio.EventInstance gateEv;

    private void Start()
    {
        gateEv = FMODUnity.RuntimeManager.CreateInstance(gateSound);
    }

    public void ShakeScreen()
    {

        iTween.ShakePosition(Camera.main.gameObject,screenShakeIntensity,screenShakeTime);

    }

    public void GateSound()
    {
        gateEv.start();
    }
}
