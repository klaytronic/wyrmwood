﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class Wave
{
	public List<Phase> phases;
}

[System.Serializable]
public class Phase
{
	public float startTime;
	public List<WaveEnemy> enemies;
	public CardAquisition phaseReward;
}

[System.Serializable]
public class WaveEnemy
{
	[ValueDropdown("GetEnemyList")]
	public int enemyType;

	[ValueDropdown("GetSpawnList")]
	public int spawnPoint;

	GameObject[] enemyList;

	Transform[] spawnList;

	public void UpdateEnemyList(GameObject[] NewEnemyList)
	{
		enemyList = NewEnemyList;
	}

	public void UpdateSpawnList(Transform[] newSpawns)
	{
		spawnList = newSpawns;
	}

	private IList<ValueDropdownItem<int>> GetEnemyList()
	{
		ValueDropdownList<int> list = new ValueDropdownList<int>();

		if (enemyList == null)
			enemyList = WaveSystem.Instance.enemyTypes;
		for (int i = 0; i < enemyList.Length; ++i)
			list.Add(enemyList[i].name, i);

		return list;
	}

	private IList<ValueDropdownItem<int>> GetSpawnList()
	{
		ValueDropdownList<int> list = new ValueDropdownList<int>();

		if (spawnList == null)
			spawnList = WaveSystem.Instance.spawns;

		for (int i = 0; i < spawnList.Length; ++i)
			list.Add(spawnList[i].name, i);

		list.Add("Random", list.Count);

		return list;
	}
}
