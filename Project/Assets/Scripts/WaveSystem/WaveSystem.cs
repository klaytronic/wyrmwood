﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;


[System.Serializable]
public class WaveSystem : Singleton<WaveSystem>
{
    [FMODUnity.EventRef]
    public string waveSound = "event:/WaveOver";

    public FMOD.Studio.EventInstance waveEv;

    [Title("Data")]
	[Button("Auto-Find Spawn Points")]
	[PropertyOrder(0)]
	void GetSpawnPoints()
	{
		Transform parent = transform.GetChild(0);
		spawns = new Transform[parent.childCount];

		for (int i = 0; i < parent.childCount; ++i)
			spawns[i] = parent.GetChild(i);

		UpdateSpawns();
	}

	[PropertyOrder(1)]
	[TabGroup("Enemy Types")]
	[AssetsOnly]
	[OnValueChanged("UpdateEnemyList")]
	public GameObject[] enemyTypes;

	[PropertyOrder(1)]
	[TabGroup("Spawn Points")]
	[SceneObjectsOnly]
	[OnValueChanged("UpdateSpawns")]
	public Transform[] spawns;

	[PropertyOrder(1)]
	[TabGroup("Timing")]
	[SerializeField] float phaseGapTiming;
	[PropertyOrder(1)]
	[TabGroup("Timing")]
	[SerializeField] float waveGapTiming;

	[Title("Wave Sequence")]
	[PropertyOrder(2)]
	public List<Wave> waves = new List<Wave>();

	int enemiesRemaining;

    public UnityEvent WaveSystemFinish;

    //alter effects
    public GameObject alterLight;
    Animator alterAnimator;
    public ParticleSystem alterParticles;
    ParticleSystem.VelocityOverLifetimeModule velocityModule;
    ParticleSystem.NoiseModule noiseModule;
    // Start is called before the first frame update
    void Start()
	{
		UpdateEnemyList();
		UpdateSpawns();

        waveEv = FMODUnity.RuntimeManager.CreateInstance(waveSound);
        alterAnimator = alterLight.GetComponent<Animator>();
        velocityModule = alterParticles.velocityOverLifetime;
        noiseModule = alterParticles.noise;
    }

	private void OnDisable()
	{
		UpdateEnemyList();
		UpdateSpawns();
	}

	void UpdateEnemyList()
	{
		for (int waveIndex = 0; waveIndex < waves.Count; ++waveIndex)
		{
			for (int phaseIndex = 0; phaseIndex < waves[waveIndex].phases.Count; ++phaseIndex)
			{
				for (int enemyIndex = 0; enemyIndex < waves[waveIndex].phases[phaseIndex].enemies.Count; ++enemyIndex)
					waves[waveIndex].phases[phaseIndex].enemies[enemyIndex].UpdateEnemyList(enemyTypes);
			}
		}
	}

	void UpdateSpawns()
	{
		for (int waveIndex = 0; waveIndex < waves.Count; ++waveIndex)
		{
			for (int phaseIndex = 0; phaseIndex < waves[waveIndex].phases.Count; ++phaseIndex)
			{
				for (int enemyIndex = 0; enemyIndex < waves[waveIndex].phases[phaseIndex].enemies.Count; ++enemyIndex)
					waves[waveIndex].phases[phaseIndex].enemies[enemyIndex].UpdateSpawnList(spawns);
			}
		}
	}

	public void StartSystem()
	{
		// todo: alert player to wave mode start

		StartCoroutine(WaveSequence(0));
	}

	IEnumerator WaveSequence(int waveIndex)
	{
		Phase[] phases = waves[waveIndex].phases.ToArray();
		WaveEnemy[] enemies = new WaveEnemy[0];
		enemiesRemaining = 0;

		// for each phase...
		for (int phaseIndex = 0; phaseIndex < phases.Length; ++phaseIndex)
		{ 
			// if this is not the first phase of this wave, wait for the last phase to be completed
			if (phaseIndex != 0)
			{

				while (enemiesRemaining > 0)
					yield return null;

                // todo: alert player to phase end
            }

            // give reward of last phase if there was a last phase and there is a reward, and wait until it has been picked up
            if (phaseIndex > 0)
			{
				if (phases[phaseIndex - 1].phaseReward != null)
				{
                    //turn on the alter light
                    alterAnimator.Play("AlterOn");
                    

                    //stop the emission on the particle system
                    alterParticles.enableEmission = true;
                    velocityModule.speedModifier = 1;
                    noiseModule.strength = .25f;
                    Debug.Log("This is where the gong should sound");
                    waveEv.start();
                    phases[phaseIndex - 1].phaseReward.gameObject.SetActive(true);

					while (phases[phaseIndex - 1].phaseReward)
						yield return null;
				}
			}

			yield return new WaitForSeconds(phaseGapTiming);

            //turn off the alter light and particles
            alterAnimator.Play("AlterOff");
            alterParticles.enableEmission = false;

            
            velocityModule.speedModifier = 0.5f;
            noiseModule.strength = 1f;
            // todo: play sounds to alert upcoming phase start

            // get enemies for this phase
            enemies = phases[phaseIndex].enemies.ToArray();
			enemiesRemaining = enemies.Length;

			// spawn enemies, add listeners to their wee lil deaths
			for (int enemyIndex = 0; enemyIndex < enemies.Length; ++enemyIndex)
			{
				Vector2 tempPos = enemies[enemyIndex].spawnPoint < spawns.Length ? spawns[enemies[enemyIndex].spawnPoint].position : spawns[Random.Range(0, spawns.Length)].position;

                yield return new WaitForSeconds(1);

                GameObject temp = Instantiate(enemyTypes[enemies[enemyIndex].enemyType], tempPos, Quaternion.identity);
				
				temp.GetComponentInChildren<EnemyHealth>().DeathEvent.AddListener(EnemyDied);           
			}
		}

		// for the last phase, wait for phase enemies to be cleared
		while (enemiesRemaining > 0)
			yield return null;

		// todo alert player to wave end

		// todo: add design control to enable healing area 

		// if there is a final phase reward, do it, wait for end
		if (phases[phases.Length - 1].phaseReward != null)
		{
            //turn on the alter light
            alterAnimator.Play("AlterOn");
            alterParticles.enableEmission = true;
            Debug.Log("This is where the gong should sound");
            waveEv.start();
            phases[phases.Length - 1].phaseReward.gameObject.SetActive(true);

			while (phases[phases.Length - 1].phaseReward)
				yield return null;
		}

        //turn off the alter light and particles
        alterAnimator.Play("AlterOff");
        alterParticles.enableEmission = false;

        // heal player
        PlayerController.Instance.health.ReceiveHealing(Mathf.Floor(PlayerController.Instance.health.maxHealth / 2));

		// start up next wave if there is one
		if (waveIndex < waves.Count - 1)
        {
            // todo: alert player to wave start
            yield return new WaitForSeconds(waveGapTiming);
            StartCoroutine(WaveSequence(waveIndex + 1));
        }
		else
		{
			// waves are all done!
			Debug.Log("wowie!!!");
            yield return new WaitForSeconds(2);
            WaveSystemFinish.Invoke();
		}
	}

	void EnemyDied()
	{
		enemiesRemaining--;
	}
}



