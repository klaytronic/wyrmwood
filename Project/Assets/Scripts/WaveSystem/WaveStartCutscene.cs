﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveStartCutscene : Cutscene
{
    [SerializeField] GameObject[] flySprites;
    public GameObject GatePoles;
    Animator gatePolesAnimator;

    public Vector3 screenShakeAmount;
    public float screenShakeTime = 1;

    private void Awake()
    {
        gatePolesAnimator = GatePoles.GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
			GetComponent<Collider2D>().enabled = false;
			StartCutscene();
		}
	}

    public IEnumerator SpawnFlySprites()
    {
        foreach (GameObject fly in flySprites)
            fly.SetActive(true);
        yield break;
    }

    public IEnumerator RemoveFlySprites()
    {
        foreach (GameObject fly in flySprites)
            Destroy(fly);
        yield break;
    }

    public IEnumerator CloseGate()
    {
        gatePolesAnimator.Play("GateClosing");
        yield break;

    }
    public IEnumerator ShakeScreen()
    {
        iTween.ShakePosition(Camera.main.gameObject, screenShakeAmount, screenShakeTime);
        yield break;
    }

    public IEnumerator StartSystem()
    {
        PlayerController.Instance.ToggleInput(true);
        ServiceLocator.Instance.cardSystem.ToggleInput(true);

        GetComponent<WaveSystem>().StartSystem();
        yield break;
    }
}


