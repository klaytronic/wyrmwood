﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShake : MonoBehaviour
{
    public Vector3 ScreenShakeIntensity;
    public float ScreenShakeTime;

    // Start is called before the first frame update
    void Start()
    {
        iTween.ShakePosition(Camera.main.gameObject,ScreenShakeIntensity,ScreenShakeTime);
    }


}
