﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test_door : MonoBehaviour
{
    [SerializeField] int sceneIndex;
	LevelManager loader;

	private void Start()
	{
        loader = ServiceLocator.Instance.levelManager;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Player")
			loader.InitializeLoad(sceneIndex);
	}
}
