﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageEnemy : MonoBehaviour
{
    public int damage;

	public bool melee;
	public bool cleave;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy" && other.GetComponentInChildren<HealthBase>())
        {
            other.GetComponentInChildren<HealthBase>().ReceiveDamage(damage);
			ServiceLocator.Instance.hitstun.HitEnemy(other.gameObject, damage, melee);
			if (!cleave)
				Destroy(gameObject);
        }
    }
}
