﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class MovePlayer : CardEffect
{
	[Header("Movement")]
	public bool rotateRelativeToPlayer;
	public bool invulnerability;
	public bool dashAnim;
	public Vector2 target;
	public float moveTime;
	public AnimationCurve easing;

	float timer;
	Vector2 startPos, targetPos;
	Vector3 temp;

	public override IEnumerator Invoke()
	{
		completed = false;
		Transform player = PlayerController.Instance.transform;
		Collider2D coll = player.gameObject.GetComponentInChildren<Collider2D>();
		Rigidbody2D rb = player.gameObject.GetComponentInChildren<Rigidbody2D>();

		timer = 0;
		while (timer < startLag)
		{
			timer += Time.deltaTime;
			yield return 0;
		}

		startPos = player.position;

		temp = rotateRelativeToPlayer ? PlayerController.Instance.reticleParent.transform.TransformPoint(target) : new Vector3(target.x, target.y, 0) + player.position;

		targetPos = temp;

		PlayerController.Instance.hurtbox.enabled = !invulnerability;

		if (dashAnim)
		{
			PlayerController.Instance.animLock = true;
			PlayerController.Instance.anim.SetFloat("AimX", (targetPos - startPos).x);
			PlayerController.Instance.anim.SetFloat("AimY", (targetPos - startPos).y);
			PlayerController.Instance.reflectionAnim.SetFloat("AimX", (targetPos - startPos).x);
			PlayerController.Instance.reflectionAnim.SetFloat("AimY", (targetPos - startPos).y);

		}


		timer = 0;
		Vector2 lastPos = rb.position;
		while (timer < moveTime)
		{
			if (dashAnim)
			{
				PlayerController.Instance.anim.Play("Dash");
				PlayerController.Instance.reflectionAnim.Play("Dash");
			}

			// find the next position to be at, apply speed needed to get there
			Vector2 nextPos = Vector2.LerpUnclamped(startPos, targetPos, easing.Evaluate(timer / moveTime));

			if (Time.timeScale != 0)
				rb.velocity = (nextPos - lastPos) / Time.deltaTime;
			else
				rb.velocity = Vector2.zero;

			timer += Time.deltaTime;
			lastPos = nextPos;
			yield return null;
		}

		rb.velocity = Vector2.zero;
		PlayerController.Instance.hurtbox.enabled = true;
		PlayerController.Instance.animLock = false;

		timer = 0;
		while (timer < endLag)
		{
			timer += Time.deltaTime;
			yield return 0;
		}

		completed = true;
	}
}

 public class MovePlayerData : CardEffectData
{
	[Header("Movement")]
	[HideInInspector] public bool rotateRelativeToPlayer;
	string rotateName;
	[PropertyTooltip("Press to toggle element's rotation.")] [Button("$rotateName", ButtonSizes.Small), GUIColor("RotateColor")]
	[ButtonGroup]
	void ToggleSpawn()
	{
		rotateRelativeToPlayer = !rotateRelativeToPlayer;

	}
	Color RotateColor()
	{
		rotateName = rotateRelativeToPlayer ? "Move Relative to Player" : "Move in World Space";
		return rotateRelativeToPlayer ? Color.yellow : Color.cyan;
	}

	[HideInInspector] public bool invulnerability;
	string invulnName;
	[PropertyTooltip("Press to toggle element's rotation.")] [Button("$invulnName", ButtonSizes.Small), GUIColor("InvulnColor")] [ButtonGroup]
	void ToggleInvuln()
	{
		invulnerability = !invulnerability;		
	}
	Color InvulnColor()
	{
		invulnName = invulnerability ? "Player Invulnerable" : "Player Vulnerable";
		return invulnerability ? Color.green : Color.red;
	}

	[HideInInspector] public bool dashAnim;
	string dashName;
	[PropertyTooltip("Press to toggle if the PC enters dash animation.")]
	[Button("$dashName", ButtonSizes.Small), GUIColor("DashColor")]
	[ButtonGroup]
	void ToggleDashAnim()
	{
		dashAnim = !dashAnim;
	}
	Color DashColor()
	{
		dashName = dashAnim ? "Use Dash Anim" : "Don't Use Dash Anim";
		return dashAnim ? Color.green : Color.red;
	}

	public Vector2 target;
	public float moveTime;
	public AnimationCurve easing;

	public void PullData(MovePlayer data)
	{ 
		activationCondition = data.condition;
		startLag = data.startLag;
		endLag = data.endLag;
		waitToFinish = data.waitToFinish;
		rotateRelativeToPlayer = data.rotateRelativeToPlayer;
		invulnerability = data.invulnerability;
		target = data.target;
		moveTime = data.moveTime;
		easing = data.easing;
	}
}
