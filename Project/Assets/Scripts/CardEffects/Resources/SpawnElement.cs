﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class SpawnElement : CardEffect
{
	[Header("Element")]
	public GameObject element;
	public bool directional;
	public bool spawnAtCursor;

	float timer;
	Quaternion rotation;
	Vector2 targetPos;

	public override IEnumerator Invoke()
	{
		completed = false;

		timer = 0;
		while(timer < startLag)
		{
			timer += Time.deltaTime;
			yield return 0;			
		}

		targetPos = spawnAtCursor ? PlayerController.Instance.reticleParent.transform.GetChild(0).position : PlayerController.Instance.transform.position;

		if (directional)
			Instantiate(element, targetPos, PlayerController.Instance.reticleParent.transform.rotation);
		else
			Instantiate(element, targetPos, Quaternion.identity);

		timer = 0;
		while (timer < endLag)
		{
			timer += Time.deltaTime;
			yield return 0;		
		}

		completed = true;
	}
}

public class SpawnElementData : CardEffectData 
{
	[Header("Element Data")]
	[HideInInspector] public bool projOrElem;
	string typeName;
	[PropertyOrder(-1)] [PropertyTooltip("Press to toggle between an element's type:\nProjectile: this element deals damage. \nVisual: This element does not deal any damage.")] [Button("$typeName", ButtonSizes.Medium), GUIColor("TypeColor")]
	void ToggleType()
	{
		projOrElem = !projOrElem;
	}
	Color TypeColor()
	{
		typeName = projOrElem ? "Type: Visual" : "Type: Projectile";
		return projOrElem ? Color.yellow : Color.cyan;
	}

	[HideIf("projOrElem")] public int damage;

	[HideInInspector] public bool melee;
	string meleeName;
	[HideIf("projOrElem")]
	[PropertyOrder(-1)]
	[PropertyTooltip("Press to toggle between a projectile's type:\nMelee: this projectile is a melee attack. \nRanged: This projectile is ranged.")]
	[ButtonGroup("Proj")]
	[Button("$meleeName", ButtonSizes.Medium), GUIColor("ProjColor")]
	void ToggleProjectileType()
	{
		melee = !melee;
	}
	Color ProjColor()
	{
		meleeName = melee ? "Melee Attack" : "Ranged Attack";
		return Color.yellow;
	}

	[HideInInspector] public bool cleave;
	string cleaveName;
	[HideIf("projOrElem")]
	[PropertyOrder(-1)]
	[ButtonGroup("Proj")]
	[PropertyTooltip("Press to toggle whether or not this projectile is not destroyed upon touching an enemy.")]
	[Button("$cleaveName", ButtonSizes.Medium), GUIColor("CleaveColor")]
	void ToggleProjectileCleave()
	{
		cleave = !cleave;
	}
	Color CleaveColor()
	{
		cleaveName = cleave ? "Cleave" : "No Cleave";
		return Color.yellow;
	}

	[InfoBox("Leave the element gameObject empty if you want a default element to be created with this card!", InfoMessageType.Info)]
	public GameObject element;

	[HideInInspector] public bool directional;
	string dirName;
	[PropertyTooltip("Press to toggle element's rotation.")] [Button("$dirName", ButtonSizes.Small), GUIColor("DirectionColor")] [ButtonGroup]
	void ToggleDirection()
	{
		directional = !directional;
		
	}
	Color DirectionColor()
	{
		dirName = directional ? "Facing Forward" : "No Rotation";
		return directional ? Color.yellow : Color.cyan;
	}

	[HideInInspector] public bool spawnAtCursor;
	string spawnName;
	[PropertyTooltip("Press to toggle element's rotation.")] [Button("$spawnName", ButtonSizes.Small), GUIColor("SpawnColor")] [ButtonGroup]
	void ToggleSpawn()
	{
		spawnAtCursor = !spawnAtCursor;

	}
	Color SpawnColor()
	{
		spawnName = spawnAtCursor ? "At Reticle Position" : "At Player Position";
		return spawnAtCursor ? Color.magenta : Color.green; 
	}

	public void PullData(SpawnElement data)
	{
		activationCondition = data.condition;
		startLag = data.startLag;
		endLag = data.endLag;
		waitToFinish = data.waitToFinish;
		element = data.element;

		if (element)
		{
			DamageEnemy temp = element.GetComponentInChildren<DamageEnemy>();
			
			if (temp)
			{
				projOrElem = false;
				damage = temp.damage;
				melee = temp.melee;
				cleave = temp.cleave;
			}
			else
				projOrElem = true;
		}

		directional = data.directional;

		spawnAtCursor = data.spawnAtCursor;
	}
}
