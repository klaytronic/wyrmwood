﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;

public class CardAquisition : MonoBehaviour
{

	[ValueDropdown("GetCardList")] public List<int> cards;
	List<GameObject> containers;

	[SerializeField] int numberOfRewards = 1;
	[SerializeField] GameObject cardButtonPrefab;
	[SerializeField] float unselectedSize;
	[SerializeField] float selectedSize;
    public float fadeDuration = 1f;
    CanvasGroup group;

	GameObject cardRewardUI, temp;
	int index, oldIndex;
	bool selectRightPressed, selectLeftPressed, running;

	[System.NonSerialized] public bool completed;

	CardSystem cardSystem;
    
    [FMODUnity.EventRef]
    public string switchcard = "event:/SwitchCard";
    
    public FMOD.Studio.EventInstance switchEv;

    CardDissolve m_cardDissolve;
    // Start is called before the first frame update
    void Start()
    {
        switchEv = FMODUnity.RuntimeManager.CreateInstance(switchcard);
        containers = new List<GameObject>();
		cardSystem = ServiceLocator.Instance.cardSystem;
        m_cardDissolve = GetComponent<CardDissolve>();
    }

	public IList<ValueDropdownItem<int>> GetCardList()
	{
		ValueDropdownList<int> list = new ValueDropdownList<int>();

		for (int i = 0; i < CardDB.Instance.cards.Count; ++i)
			list.Add(CardDB.Instance.cards[i].name, i);

		return list;
	}

	public IEnumerator StartAcquisition()
	{
        //reset the dissolve
        m_cardDissolve.ResetFade();

		completed = false;
        cardRewardUI = ServiceLocator.Instance.cardAquisitionUI;
        group = cardRewardUI.transform.parent.GetComponent<CanvasGroup>();
        group.alpha = 0;
        cardRewardUI.SetActive(true);

		PlayerController.Instance.ToggleInput(false);
		cardSystem.ToggleInput(false);
		PlayerController.Instance.StopDash();

		for (int i = 0; i < cards.Count; ++i)
		{
			containers.Add(Instantiate(cardButtonPrefab, cardRewardUI.transform));
			CardInfo tempInfo = ServiceLocator.Instance.cardSystem.availCards[cards[i]].GetComponent<CardProperties>().info;

            containers[i].transform.GetChild(0).GetComponent<Image>().sprite = ServiceLocator.Instance.cardSystem.manaFrames[tempInfo.manaCost-1];

            containers[i].GetComponentInChildren<Image>().sprite = tempInfo.fullImage;
			containers[i].transform.GetChild(1).GetComponent<TMP_Text>().text = tempInfo.title;
			containers[i].transform.GetChild(2).GetComponent<TMP_Text>().text = tempInfo.description;
            containers[i].transform.GetChild(1).GetComponent<TMP_Text>().enabled = false;
            containers[i].transform.GetChild(2).GetComponent<TMP_Text>().enabled = false;
            containers[i].transform.localScale = Vector3.one * unselectedSize;
		}


        // add an empty choice
        containers.Add(Instantiate(cardButtonPrefab, cardRewardUI.transform));
        containers[cards.Count].transform.GetChild(0).GetComponent<Image>().sprite = ServiceLocator.Instance.cardSystem.manaFrames[0];
        containers[cards.Count].GetComponentInChildren<Image>().sprite = ServiceLocator.Instance.cardSystem.NoArt;
        containers[cards.Count].transform.GetChild(0).GetComponent<Image>().color = Color.red;
        containers[cards.Count].transform.GetChild(1).GetComponent<TMP_Text>().text = "None";
        containers[cards.Count].transform.GetChild(2).GetComponent<TMP_Text>().text = "Skip this set of cards.";
        containers[cards.Count].transform.GetChild(1).GetComponent<TMP_Text>().enabled = false;
        containers[cards.Count].transform.GetChild(2).GetComponent<TMP_Text>().enabled = false;
        containers[cards.Count].transform.localScale = Vector3.one * unselectedSize;


        index = cards.Count / 2;
		oldIndex = index;
		SetSelection();

        iTween.ValueTo(this.gameObject, iTween.Hash("from", 0,
                                                    "to", 1,
                                                    "time", fadeDuration,
                                                    "onupdatetarget", this.gameObject,
                                                    "onupdate", "UpdateFade"));

		running = true;

		while (true)
		{
			if (Input.GetButton("Select_Right") && !selectRightPressed)
			{
				selectRightPressed = true;
				index = (index + 1) % (cards.Count + 1);
				SetSelection();
			}
			else if (!Input.GetButton("Select_Right"))
				selectRightPressed = false;

			if (Input.GetButtonDown("Select_Left") && !selectLeftPressed)
			{
				selectLeftPressed = true;
				index = index - 1 < 0 ? cards.Count : index - 1;
				SetSelection();
			}
			else if (!Input.GetButton("Select_Left"))
				selectLeftPressed = false;

			if (Input.GetButtonDown("Dash") || Input.GetButton("Jump"))
			{
                if (index == cards.Count)
                {
                    break;
                }
                else
                {
                    cardSystem.deckBuild.Add(cards[index]);
                    cardSystem.liveDeck.Add(ServiceLocator.Instance.cardSystem.availCards[cards[index]]);
                    //temp = Instantiate(ServiceLocator.Instance.cardSystem.availCards[cards[index]], containers[index].transform.position, Quaternion.identity, ServiceLocator.Instance.fade.transform);

                    GameObject mini = cardSystem.MoveCard(containers[index].transform.position, cardSystem.deckTransform.position);

                    //destroy selected card
                    Destroy(containers[index]);


                    while (mini)
                        yield return null;

                    cardSystem.RequestFillHand();
                    break;
                }
			}
			yield return 0;
		}

        //start disolving
        if (m_cardDissolve.dissolveMaterial.GetFloat("_Fade") == 1)
        {
            m_cardDissolve.canDissolve = true;

        }

        while (m_cardDissolve.dissolveMaterial.GetFloat("_Fade") >= 0.1)
        {
     
            yield return null;
        }
        foreach (Transform card in cardRewardUI.transform)
            Destroy(card.gameObject);

        //reset the dissolve effect
        m_cardDissolve.canDissolve = false;
        m_cardDissolve.ResetFade();

		cardRewardUI.SetActive(false);
		PlayerController.Instance.ToggleInput(true);
		ServiceLocator.Instance.cardSystem.ToggleInput(true);

		while (temp)
			yield return 0;

		completed = true;
		yield break;
	}



	void SetSelection()
	{
        switchEv.start();
		containers[index].transform.localScale = Vector3.one * selectedSize;
        containers[index].transform.GetChild(1).GetComponent<TMP_Text>().enabled = true;
        containers[index].transform.GetChild(2).GetComponent<TMP_Text>().enabled = true;
        if (oldIndex != index)
        {
            containers[oldIndex].transform.localScale = Vector3.one * unselectedSize;
            containers[oldIndex].transform.GetChild(1).GetComponent<TMP_Text>().enabled = false;
            containers[oldIndex].transform.GetChild(2).GetComponent<TMP_Text>().enabled = false;
        }

		oldIndex = index;
	}

    void UpdateFade(float alpha)
    {
        if (group)
            group.alpha = alpha;
    }

   
}
