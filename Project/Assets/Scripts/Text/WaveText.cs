﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WaveText : MonoBehaviour
{
	Animator[] anims;
	TMP_Text[] text;
	public float offset = 0.1f;
	public float fadeInDuration = 3f;

    CanvasGroup group;

    public bool autoPlay = false;

    // Start is called before the first frame update
    void Start()
    {
		anims = GetComponentsInChildren<Animator>();
		text = GetComponentsInChildren<TMP_Text>();
        group = GetComponent<CanvasGroup>();
        group.alpha = 0;

        StartCoroutine(WaveSequence());

        if (autoPlay)
            Invoke("FadeInText", 2f);
    }

	IEnumerator WaveSequence()
	{
		for (int i = 0; i < anims.Length; ++i)
		{
			anims[i].Play("Wave");

			float timer = 0;
			while (timer < offset)
			{
				timer += Time.deltaTime;
				yield return null;
			}		
		}
	}

	public void FadeInText()
	{
        StartCoroutine(WaveSequence());
		StartCoroutine(FadeInTextSequence());
	}

    public void FadeOutText()
    {
        StartCoroutine(FadeOutTextSequence());
    }

	IEnumerator FadeInTextSequence()
	{
        iTween.ValueTo(this.gameObject, iTween.Hash("from", group.alpha,
                                                    "to", 1,
                                                    "time", fadeInDuration,
                                                    "onupdate", "UpdateFade",
                                                    "onupdatetarget", this.gameObject));
        yield break;
	}

    IEnumerator FadeOutTextSequence()
    {
        iTween.ValueTo(this.gameObject, iTween.Hash("from", group.alpha,
                                            "to", 0,
                                            "time", fadeInDuration,
                                            "onupdate", "UpdateFade",
                                            "onupdatetarget", this.gameObject,
                                            "oncomplete", "Finish",
                                            "oncompletetarget", this.gameObject));

        yield break;
    }

	IEnumerator UpdateFade(float alpha)
	{
        group.alpha = alpha;
		yield break;
	}

    void Finish()
    {
        for (int i = 0; i < anims.Length; ++i)
            anims[i].Play("Idle");
    }
}
