﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class CameraControl : MonoBehaviour
{
	public float trackRate = 5f;
	Transform[] views;
	public Transform target;
	Vector3 targetPos;
	float magnifier;

	private void Start()
	{
		Camera.main.transparencySortMode = TransparencySortMode.CustomAxis;
		Camera.main.transparencySortAxis = Vector3.up;
	}

	public void ReturnToPlayer()
	{
		target = PlayerController.Instance.transform;
		Debug.Log("Camera returned to player");
	}

	// Update is called once per frame
	void Update()
    {
		if (target)
		{
			targetPos = new Vector3(target.position.x, target.position.y, transform.position.z);

			magnifier = (Vector3.Distance(transform.position, targetPos) / trackRate) * Time.deltaTime;

			transform.position = Vector3.MoveTowards(transform.position, targetPos, magnifier);
		}
    }
}

[System.Serializable]
public class CameraAction
{
    public bool returnToPlayer;

    [HideIf("returnToPlayer")]
    public Transform target;

    public float moveSpeed;

    public string customMethodName;
}
