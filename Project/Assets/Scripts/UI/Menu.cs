﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Menu : MonoBehaviour
{
    public GameObject eventSystem;
    public EventSystem m_eventSystem;

    LevelManager levelManager;

    // Start is called before the first frame update
    void Start()
    {
        m_eventSystem = eventSystem.GetComponent<EventSystem>();
        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
    }

    public void StartGame(int ScenetoLoad)
    {
        eventSystem.SetActive(false);
        levelManager.InitializeLoad(ScenetoLoad);
    }

}
