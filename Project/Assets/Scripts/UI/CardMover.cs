﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardMover : MonoBehaviour
{
    [SerializeField] float duration = 1f;
    [SerializeField] float upwardsDistance = 2f;
    [SerializeField] AnimationCurve FirstYEval;
    [SerializeField] AnimationCurve SecondYEval;
    [SerializeField] AnimationCurve xEval;

    //public UISoundManager UIman;

    private void Start()
    {
        //UIman = GameObject.Find("UISoundManager").GetComponent<UISoundManager>();
    }

    public void MoveTo(Vector2 target)
    {
        StartCoroutine(MovingSequence(target));
        //UIman.DrawCard();
    }

    IEnumerator MovingSequence(Vector2 target)
    {
        float timer = 0;
        Vector2 currentPos;
        Vector2 startPos = transform.position;
        while (timer <= duration / 2)
        {
            currentPos.x = Mathf.Lerp(startPos.x, target.x, xEval.Evaluate(timer / duration));

            currentPos.y = Mathf.Lerp(startPos.y, startPos.y + upwardsDistance, FirstYEval.Evaluate(timer / (duration / 2)));

            float angle = Mathf.Atan2((currentPos.y - transform.position.y), (currentPos.x - transform.position.x)) * Mathf.Rad2Deg - 90;
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

            transform.position = currentPos;

            timer += Time.deltaTime;
            yield return null;
        }

        startPos = transform.position;
        while (timer <= duration)
        {
            currentPos.x = Mathf.Lerp(startPos.x, target.x, xEval.Evaluate(timer / duration));

            currentPos.y = Mathf.Lerp(startPos.y, target.y, SecondYEval.Evaluate((timer - (duration / 2)) / (duration / 2)));

            float angle = Mathf.Atan2((currentPos.y - transform.position.y), (currentPos.x - transform.position.x)) * Mathf.Rad2Deg - 90;
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

            transform.position = currentPos;

            timer += Time.deltaTime;
            yield return null;
        }

        transform.position = target;
        Destroy(this.gameObject);

        yield break;
    }
}
