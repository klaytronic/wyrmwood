﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class HealthBarController : MonoBehaviour
{
    public Slider healthSlider;
    public int currentHP;

    void Awake()
    {
        healthSlider = GetComponent<Slider>();
    }

    public void updateHP (int HP)
    {
        currentHP = HP;
        healthSlider.value = currentHP;
    }
}

