﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

public enum InputButton { Dash, Mulligan, Cast, Select, Move, Aim, Unique }

public class TutorialUI : MonoBehaviour
{
    WaveText wave;
    public bool autoPlay = false;
    public float promptDelay = 0;
    public bool specialExit = false;

    bool prompted = false;
    bool completed = false;

    public UnityEvent promptStart;
    public UnityEvent promptFinish;
    
    public InputButton buttonPrompt;
    string inputButton;

    // Start is called before the first frame update
    void Start()
    {
        wave = GetComponent<WaveText>();

        if (autoPlay)
            StartCoroutine(PromptSequence());

        switch (buttonPrompt)
        {
            case InputButton.Dash:
                inputButton = "Dash";
                break;
            case InputButton.Mulligan:
                inputButton = "Mulligan";
                break;
        }
    }
    public void Prompt()
    {
        StartCoroutine(PromptSequence());
    }

    IEnumerator PromptSequence()
    {
        if (completed)
            yield break;
        yield return new WaitForSeconds(promptDelay);
        wave.FadeInText();
        ServiceLocator.Instance.ButtonPrompt(buttonPrompt);
        promptStart.Invoke();
        if (!specialExit)
            StartCoroutine(PressCheck());
    }

    public void UnPrompt()
    {
        StartCoroutine(UnPromptSequence());
    }

    IEnumerator UnPromptSequence()
    {
        wave.FadeOutText();
        ServiceLocator.Instance.ButtonUnprompt();
        promptFinish.Invoke();
        completed = true;
        yield break;
    }

    IEnumerator PressCheck()
    {
        prompted = true;

        while (prompted)
        {
            if (inputButton == null)
            {
                switch (buttonPrompt)
                {
                    case InputButton.Select:
                        if (Input.GetButtonDown("Select_Right") || Input.GetButtonDown("Select_Left"))
                            prompted = false;
                        break;
                    case InputButton.Move:
                        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
                            prompted = false;
                        break;
                    case InputButton.Aim:
                        if (Input.GetAxis("Aim_Horz") != 0 || Input.GetAxis("Aim_Vert") != 0)
                            prompted = false;
                        break;
                    case InputButton.Cast:
                        if (Input.GetAxis("Cast") != 1)
                            prompted = false;
                        break;
                }
            }
            else if (Input.GetButtonDown(inputButton))
                prompted = false;

            if (!prompted)
                yield return StartCoroutine(UnPromptSequence());

            yield return null;
        }
    }
}
