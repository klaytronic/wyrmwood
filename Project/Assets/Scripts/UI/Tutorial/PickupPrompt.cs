﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupPrompt : MonoBehaviour
{
    public TutorialUI tutorial;

    private void OnDisable()
    {
        if (tutorial)
            tutorial.Prompt();
    }
}
