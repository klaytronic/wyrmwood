﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnPromptOnDestroy : MonoBehaviour
{
    public TutorialUI tutorial;

    private void OnDisable()
    {
        tutorial.UnPrompt();
    }
}
