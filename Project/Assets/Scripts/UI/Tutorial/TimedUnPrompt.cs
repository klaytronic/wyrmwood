﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedUnPrompt : MonoBehaviour
{
    public float duration;
    public TutorialUI tutorial;

    public void StartTimer()
    {
        StartCoroutine(StartTimerSequence());
    }

    IEnumerator StartTimerSequence()
    {
        yield return new WaitForSeconds(duration);
        tutorial.UnPrompt();
    }
}
