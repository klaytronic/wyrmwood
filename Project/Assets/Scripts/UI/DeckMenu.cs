﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;

public class DeckMenu : MonoBehaviour
{
    [SerializeField] GameObject menuCanvas;
	[SerializeField] GameObject cardList;
	[SerializeField] GameObject cardButtonPrefab;
	[SerializeField] GameObject cardPreview;
	[SerializeField] List<int> deck;
	[SerializeField] float unselectedSize = 1f;
	[SerializeField] float selectedSize = 1.25f;

	[SerializeField] ScrollRect cardScrollView;

	Image previewImage, previewFrame;
	TMP_Text previewTitle, previewManaCost, previewDesc;

	GameObject chosenCard;

	bool previewing, toggle;
	int currentRow, index, oldIndex;
	public float moveRate;
	public float barLerpTime;
	float timer;

	Vector2 inputVector;
	float scrollBarMovement;

    public UISoundManager UISound; 

	// Update is called once per frame
	private void Start()
	{
		previewImage = cardPreview.transform.GetChild(0).GetComponent<Image>();
		previewFrame = cardPreview.transform.GetChild(1).GetComponent<Image>();
		previewTitle = cardPreview.transform.GetChild(2).GetComponent<TMP_Text>();
		previewDesc = cardPreview.transform.GetChild(3).GetComponent<TMP_Text>();

		timer = moveRate;

		ToggleDeckMenu(false);
	}

	public void ToggleDeckMenu(bool temp)
    {
		toggle = temp;
		if (temp)
		{
			Time.timeScale = 0;
			menuCanvas.gameObject.SetActive(true);

			RebuildUI();

			index = 0;
			MoveSelection();

			PlayerController.Instance.ToggleInput(false);
			ServiceLocator.Instance.cardSystem.ToggleInput(false);
		}
		else
		{
			Time.timeScale = 1;
			menuCanvas.gameObject.SetActive(false);
			index = 0;
			MoveSelection();

			PlayerController.Instance.ToggleInput(true);
			ServiceLocator.Instance.cardSystem.ToggleInput(true);

			if (previewing)
				TogglePreview();
		}
    }

	private void RebuildUI()
	{
		deck = ServiceLocator.Instance.cardSystem.deckBuild;

		foreach (Transform card in cardList.transform)
			Destroy(card.gameObject);

		//instantiate a container for the card
		for (int i = 0; i < deck.Count; i++)
		{
			GameObject card = Instantiate(cardButtonPrefab, cardList.transform);
			CardInfo info = ServiceLocator.Instance.cardSystem.availCards[ServiceLocator.Instance.cardSystem.deckBuild[i]].GetComponent<CardProperties>().info;
			card.GetComponentInChildren<Image>().sprite = info.fullImage;

            card.transform.GetChild(1).GetComponent<Image>().sprite = ServiceLocator.Instance.cardSystem.manaFrames[info.manaCost - 1];

            card.GetComponentInChildren<TMP_Text>().text = info.title;
		}
	}

	private void Update()
	{
        if (Input.GetButtonDown("Menu"))
        {
            ToggleDeckMenu(!toggle);
            UISound.SwitchCard();
        }

        if (!toggle)
        {
            UISound.SwitchCard();
            return;
        }

		if (Input.GetButtonDown("Dash"))
		{
            UISound.SwitchCard();
            TogglePreview();
		}

		if (previewing)
			return;

		inputVector = new Vector2(-Input.GetAxisRaw("Scroll_Horz"), Input.GetAxisRaw("Scroll_Vert"));

		scrollBarMovement = Input.GetAxis("Vertical");

		if (timer >= moveRate)
		{
			if (inputVector.x != 0)
			{
				timer = 0;
				index = (index + (int) inputVector.x) % deck.Count;
				index = index < 0 ? index + deck.Count : index;
				MoveSelection();
			}

			if (inputVector.y != 0)
			{
				timer = 0;
				index = (index + ((int)inputVector.y * 5)) % deck.Count;
				index = index < 0 ? index + deck.Count : index;
				MoveSelection();
			}
		}
		else if (inputVector == Vector2.zero)
		{
			timer = moveRate;
		}
		else
		{
			timer += Time.deltaTime;
		}
	}

	void MoveSelection()
	{
		if (cardList.transform.childCount == 0)
			return;

		if (oldIndex != index)
			cardList.transform.GetChild(oldIndex).transform.localScale = Vector3.one * unselectedSize;

		cardList.transform.GetChild(index).transform.localScale = Vector3.one * selectedSize;

		currentRow = index / 5;

		if (currentRow != oldIndex / 5)
		{
			StopAllCoroutines();
			StartCoroutine(LerpBar());
		}

		oldIndex = index;

        UISound.SwitchCard();
		chosenCard = ServiceLocator.Instance.cardSystem.availCards[ServiceLocator.Instance.cardSystem.deckBuild[index]];
	}

	IEnumerator LerpBar()
	{
		float timer = 0;
		float target = (float) (((deck.Count - 5) / 5) - currentRow) / ((deck.Count - 5) / 5);

		if (cardScrollView.verticalScrollbar)
		{
			while (timer < barLerpTime)
			{
				cardScrollView.verticalScrollbar.value = Mathf.Lerp(cardScrollView.verticalScrollbar.value, target, timer / barLerpTime);
				timer += Time.deltaTime;
				yield return null;
			}

			cardScrollView.verticalScrollbar.value = target;
		}
	}

	void TogglePreview()
	{
		previewing = !previewing;

		cardPreview.SetActive(previewing);

		if (previewing)
		{
			CardInfo info = chosenCard.GetComponent<CardProperties>().info;

            previewFrame.sprite = ServiceLocator.Instance.cardSystem.manaFrames[info.manaCost - 1];

			previewImage.sprite = info.fullImage;
			previewTitle.text = info.title;
			previewDesc.text = info.description;
		}
	}
}
