﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeText : MonoBehaviour
{
    //TO DO:
    //Text size over time
    //Text position over time
    //Text color + size based on damage amt 

    private Text thisText;
    private CanvasRenderer thisRend; 

    public float fadeInc = 0.0001f;
    private float textAlpha = 1;

    void Start()
    {
        thisText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        textAlpha = textAlpha - fadeInc * Time.deltaTime;

        /*
        if (textAlpha < 0)
        {
            Destroy(this.gameObject);
        }
        */

        thisText.color = new Color(1f, 1f, 1f, textAlpha);
    }
}
