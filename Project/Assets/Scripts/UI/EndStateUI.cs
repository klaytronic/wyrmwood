﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndStateUI : MonoBehaviour
{
    public float fadeDuration;

    CanvasGroup canvas;

    bool running;

    public void StartEnd()
    {
        canvas = GetComponent<CanvasGroup>();
        GetComponentInChildren<WaveText>().FadeInText();
        ServiceLocator.Instance.cardSystem.ToggleInput(false);
        PlayerController.Instance.ToggleInput(false);
        iTween.ValueTo(this.gameObject, iTween.Hash("from", 0,
                                                    "to", 1,
                                                    "time", fadeDuration,
                                                    "onupdate", "UpdateAlpha",
                                                    "oncomplete", "finishAlpha"));
    }

    public void UpdateAlpha(float alpha)
    {
        canvas.alpha = alpha;
    }

    public void FinishAlpha()
    {
        canvas.alpha = 1;
    }

}
