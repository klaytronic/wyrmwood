﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitRestart : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        if (Input.GetKeyDown(KeyCode.R) || Input.GetButtonDown("Restart"))
            RestartGame();

    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void RestartGame()
    {
        Destroy(PlayerController.Instance.gameObject);
        Destroy(ServiceLocator.Instance.gameObject);
        SceneManager.LoadScene(0);
    }
}
