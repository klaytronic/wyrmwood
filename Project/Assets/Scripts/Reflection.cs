﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Reflection : MonoBehaviour
{
    
void Start()
    {
        //Create an empty gameobject
        GameObject emptyGameObject = new GameObject();
        GameObject reflectionObject = Instantiate(emptyGameObject, Vector3.zero, Quaternion.identity) as GameObject;

        //Mirror the new object on the y axis an move into position
        SpriteRenderer originalRenderer = GetComponent<SpriteRenderer>();
        float ySize = originalRenderer.sprite.bounds.size.y;
        reflectionObject.transform.parent = transform;
        reflectionObject.transform.localScale = new Vector3(1 * Mathf.Sign(transform.localScale.x),1,1);
        reflectionObject.transform.localPosition = new Vector3(0, -ySize, 0);

        //copy the sprite renderer
        reflectionObject.AddComponent<SpriteRenderer>();
        SpriteRenderer renderer = reflectionObject.GetComponent<SpriteRenderer>();
        renderer.sprite = originalRenderer.sprite;
        renderer.sortingLayerID = originalRenderer.sortingLayerID;

        //put our reflection material on the new object
        Material newMat = Resources.Load("WaterReflection", typeof(Material)) as Material;
        renderer.material = newMat;




    }
}
