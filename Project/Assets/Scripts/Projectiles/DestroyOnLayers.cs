﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnLayers : MonoBehaviour
{
	[SerializeField] LayerMask layers;
    public GameObject trailParticleSystem;

    void OnTriggerEnter2D(Collider2D other)
	{
        if (((1 << other.gameObject.layer) & layers.value) != 0)
        {
            if (trailParticleSystem)
            {
                trailParticleSystem.transform.SetParent(null);
                ParticleSystem m_particleSystem = trailParticleSystem.GetComponent<ParticleSystem>();
                m_particleSystem.Stop();
            }
            Destroy(gameObject);
        }
	}
}
