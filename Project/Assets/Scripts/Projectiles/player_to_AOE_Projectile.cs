﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_to_AOE_Projectile : MonoBehaviour
{

    public Vector3 PlayerPosition;
    public Vector3 TargetLocation;
    public Vector3 Direction;
    public Vector3 ScreenShakeIntensity;

    public GameObject ExplosionParticles;
    public GameObject ProjectilePrefab;
    public GameObject Projectile;

    public float distanceToTarget;
    public float speed;
    public float screenShakeTime = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        PlayerPosition = PlayerController.Instance.transform.position;
        TargetLocation = this.transform.position;
        Direction = (this.transform.position - PlayerPosition).normalized;
        Projectile = Instantiate(ProjectilePrefab,PlayerPosition,Quaternion.identity);

        float rot_z = Mathf.Atan2(Direction.y,Direction.x) * Mathf.Rad2Deg;
        Projectile.transform.rotation = Quaternion.Euler(0f, 0f, rot_z);

    }

    // Update is called once per frame
    void Update()
    {
        if (Projectile !=null)
        {
            Projectile.transform.Translate(Vector3.right * speed * Time.deltaTime);
            distanceToTarget = Vector3.Distance(TargetLocation, Projectile.transform.position);

            if (distanceToTarget < 1)
            {
                iTween.ShakePosition(Camera.main.gameObject,ScreenShakeIntensity,screenShakeTime);
                Destroy(Projectile);
                ExplosionParticles.SetActive(true);
                this.GetComponent<CircleCollider2D>().enabled = true;
            }
        }
       
    }
}
