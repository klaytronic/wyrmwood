﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test_projectile : MonoBehaviour
{
	[SerializeField] float speed = 1;
    [SerializeField] bool spawnAsChildOfPlayer = false;
    // Update is called once per frame
    void Update()
    {
        if (spawnAsChildOfPlayer)
        {
            transform.parent = PlayerController.Instance.gameObject.transform;
        }

		transform.Translate(Vector2.up * (speed * Time.deltaTime));
    }
}
