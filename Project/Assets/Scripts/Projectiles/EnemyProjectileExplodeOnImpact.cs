﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileExplodeOnImpact : MonoBehaviour
{
    [SerializeField]
    private string[] collisionTags;

    private Animator ac;
    private bool hasExploded = false;
    // Start is called before the first frame update
    void Start()
    {
        ac = GetComponentInChildren<Animator>();
        for (int i = 0; i < collisionTags.Length; i++)
        {

            if (collisionTags[i] == null)
            {
                Debug.LogError("collisionTag variable on object: " + transform.name + " isn't set to anything. Please fill in its tag.");
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        for (int i = 0; i < collisionTags.Length; i++)
        {
            if (collision.gameObject.CompareTag(collisionTags[i]) && !hasExploded)
            {
                hasExploded = true;
                GetComponentInChildren<DamageOnContact>().enabled = false;
                GetComponentInChildren<EnemyProjectile>().enabled = false;
                GetComponentInChildren<DestroyTimer>().enabled = false;
                ac.Play("Impact");
                Destroy(gameObject, 2);
            }
        }
    }
}
