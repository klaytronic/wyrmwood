﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD.Studio; 

public class PlayerSoundManager : MonoBehaviour
{
    Rigidbody2D playerRB;
    //FMODUnity.StudioEventEmitter emitter;


    [FMODUnity.EventRef]
    public string music = "event:/Music";

    [FMODUnity.EventRef]
    public string ambience = "event:/CaveAmbience";

    [FMODUnity.EventRef]
    public string footsteps = "event:/Footsteps";

    [FMODUnity.EventRef]
    public string dash = "event:/Dash";

    [FMODUnity.EventRef]
    public string takehit = "event:/TakeDamage";

    [FMODUnity.EventRef]
    public string heal = "event:/ReceiveHeal";

    [FMODUnity.EventRef]
    public string death = "event:/Death";

    public FMOD.Studio.EventInstance musicEv;
    public FMOD.Studio.EventInstance ambienceEv;
    public FMOD.Studio.EventInstance footstepsEv;
    public FMOD.Studio.EventInstance dashEv;
    public FMOD.Studio.EventInstance hitEv;
    public FMOD.Studio.EventInstance healEv;
    public FMOD.Studio.EventInstance deathEv;

    //float velocity;

    bool playing;
    bool dashing;

    public int musicCue = 0;

    // Start is called before the first frame update
    void Start()
    {
        playerRB = this.GetComponent<Rigidbody2D>();
        //emitter = this.GetComponent<FMODUnity.StudioEventEmitter>();

        footstepsEv = FMODUnity.RuntimeManager.CreateInstance(footsteps);
        footstepsEv.start();

        musicEv = FMODUnity.RuntimeManager.CreateInstance(music);
        musicEv.start();

        ambienceEv = FMODUnity.RuntimeManager.CreateInstance(ambience);
        ambienceEv.start();

        PlayerController.Instance.health.TookDamageEvent.AddListener(Hit);
        PlayerController.Instance.health.ReceivedHealingEvent.AddListener(Heal);
        PlayerController.Instance.health.DeathEvent.AddListener(Death);

        dashEv = FMODUnity.RuntimeManager.CreateInstance(dash);
        hitEv = FMODUnity.RuntimeManager.CreateInstance(takehit);
        healEv = FMODUnity.RuntimeManager.CreateInstance(heal);
        deathEv = FMODUnity.RuntimeManager.CreateInstance(death);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(playerRB.velocity.magnitude);

        if (playerRB.velocity.magnitude > 2.5f)
        {
            footstepsEv.setParameterByName("Velocity", 1f);
        }
        else
        {
            dashing = false;
            footstepsEv.setParameterByName("Velocity", 0f);
        }

        if ((playerRB.velocity != Vector2.zero) && !playing)
        {
            footstepsEv.start();
            playing = true;
        }
        else if ((playerRB.velocity == Vector2.zero)||(playerRB.velocity.magnitude > 6))
        {
            footstepsEv.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            playing = false;


            if (playerRB.velocity.magnitude > 5 && !dashing)
            {
                dashEv.start();
                dashing = true;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Cue")
        {
            if (musicCue < 3)
            {
                musicCue = musicCue + 1;
                musicEv.setParameterByName("Progression", musicCue);
            }
            else
            {
                musicCue = musicCue - 1;
                musicEv.setParameterByName("Progression", musicCue);
            }
        }

        if (other.gameObject.tag == "CaveExit")
        {
            ambienceEv.setParameterByName("Zone", 1f);
        }
    }

    void Hit(float damage)
    {
        hitEv.start();
    }

    void Heal(float heal)
    {
        healEv.start();
    }

    void Death()
    {
        Debug.Log("Died!");
        deathEv.start();
    }
}
