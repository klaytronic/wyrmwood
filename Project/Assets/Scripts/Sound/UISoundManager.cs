﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD.Studio;

public class UISoundManager : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string draw = "event:/DrawCard";
    [FMODUnity.EventRef]
    public string switchcard = "event:/SwitchCard";
    [FMODUnity.EventRef]
    public string manaGain = "event:/Pickup";

    public FMOD.Studio.EventInstance drawEv;
    public EventInstance switchEv;
    public EventInstance manaEv;

    private void Start()
    {
        drawEv = FMODUnity.RuntimeManager.CreateInstance(draw);
        switchEv = FMODUnity.RuntimeManager.CreateInstance(switchcard);
        manaEv = FMODUnity.RuntimeManager.CreateInstance(manaGain);
    }

    public void DrawCard()
    {
        //Debug.Log("Draw card sound should be playing");
        drawEv.start();
    }

    public void SwitchCard()
    {
        //Debug.Log("Switch card sound should be playing");
        switchEv.start();
    }

    public void ManaGain()
    {
        manaEv.start();
    }
}
    