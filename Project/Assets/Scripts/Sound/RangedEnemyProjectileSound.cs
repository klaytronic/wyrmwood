﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemyProjectileSound : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string proj = "event:/GhostProjectile";

    public FMOD.Studio.EventInstance ghostprojEv;

    [FMODUnity.EventRef]
    public string hit = "event:/GhostProjectileHit";

    public FMOD.Studio.EventInstance ghosthitEv;

    public void PlayHit()
    {
        ghostprojEv = FMODUnity.RuntimeManager.CreateInstance(proj);
        ghostprojEv.start();
    }

    public void PlayLoop()
    {
        ghosthitEv = FMODUnity.RuntimeManager.CreateInstance(hit);
        ghosthitEv.start();
    }
}

