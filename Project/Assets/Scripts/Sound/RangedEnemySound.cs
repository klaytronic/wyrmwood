﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemySound : MonoBehaviour
{

    [FMODUnity.EventRef]
    public string attack = "event:/GhostAttack";

    public FMOD.Studio.EventInstance ghostattackEv;

    [FMODUnity.EventRef]
    public string tele = "event:/GhostTeleport";

    public FMOD.Studio.EventInstance ghostteleEv;

    public void PlaySound()
    {
        ghostattackEv = FMODUnity.RuntimeManager.CreateInstance(attack);
        ghostattackEv.start();
    }

    public void TeleSound()
    {
        ghostteleEv = FMODUnity.RuntimeManager.CreateInstance(tele);
        ghostteleEv.start();
    }
}
