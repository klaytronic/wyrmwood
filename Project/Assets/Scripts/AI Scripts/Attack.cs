﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    public GameObject target;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        LineOfSightCheck();
    }

    private void LineOfSightCheck()
    {
        //RaycastHit2D hit = Physics2D.Raycast(transform.position, target.transform.position - transform.position, Vector2.Distance(target.transform.position, transform.position), LayerMask.NameToLayer("Obstacle"));
        RaycastHit2D lineOfSightHit = Physics2D.Linecast(transform.position, target.transform.position, 1 << LayerMask.NameToLayer("Obstacle"));
        if (lineOfSightHit)
        {
            Debug.Log("Hit a wall");
        }
    }
}
