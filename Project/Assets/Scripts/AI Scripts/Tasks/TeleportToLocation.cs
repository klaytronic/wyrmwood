using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class TeleportToLocation : Action
{
	public SharedGameObject teleportTarget;
	[SerializeField]
	private Vector2 teleportLocation;
	[SerializeField]
	private bool teleportByVector = false;

	public override void OnStart()
	{
		if (teleportByVector)
		{
			transform.position = teleportLocation;
		} else
		{
			transform.position = (Vector2)teleportTarget.Value.transform.position;
		}
	}

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
}