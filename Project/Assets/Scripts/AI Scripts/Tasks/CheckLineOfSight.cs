using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class CheckLineOfSight : Action
{
    public SharedGameObject targetToCheck;
	[SerializeField]
	private LayerMask layersToCheck;

	public override TaskStatus OnUpdate()
	{
        if (Physics2D.Linecast(transform.position, targetToCheck.Value.transform.position, layersToCheck))
        {
            return TaskStatus.Running;
        } else
        {
            return TaskStatus.Success;
        }
    }
}