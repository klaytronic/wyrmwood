using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class DestinationInArea : Action
{
    public SharedGameObject currentTarget;
    public SharedTransform currentDestination;

        [SerializeField]
    private float rollingRadius = 2;

    [SerializeField]
    private bool hasMinimumDistance = false;
    [SerializeField]
    private float minimumDistance = 1;

    public override void OnStart()
    {
        UpdateCurrentDestination(DestinationNearTarget());
    }

    public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}

    private Vector2 DestinationNearTarget()
    {
        //Roll a direction from the center of a circle
        Vector2 rolledDirection = Random.insideUnitCircle;

        //If we're adding a minimum distance
        if (hasMinimumDistance)
        {
            //Add that minimum distance to our calculation (So this value can't get closer than it)
            //BEDMAS Minimum distance remains the same as an offset, the outer radius is what's changed
            float length = minimumDistance + rollingRadius - minimumDistance * Random.value;

            Vector2 rolledLocation = (Vector2)currentTarget.Value.transform.position + rolledDirection.normalized * length;
            return rolledLocation;
        }
        else
        {
            Vector2 rolledLocation = (Vector2)currentTarget.Value.transform.position + rolledDirection.normalized * rollingRadius * Random.value;
            return rolledLocation;
        }

    }

    private void UpdateCurrentDestination(Vector3 newPosition)
    {
        currentDestination.Value.position = newPosition;
    }
}