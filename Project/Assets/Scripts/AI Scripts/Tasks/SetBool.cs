using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class SetBool : Action
{
    public SharedBool targetBoolean;
    [SerializeField]
    private bool valueToStore;

    public override void OnStart()
	{
        targetBoolean.Value = valueToStore;
	}

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
}