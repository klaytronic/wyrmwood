using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class MeleeAttack : Action
{
    [SerializeField]
    public SharedInt damageToInflict = 1;
    [SerializeField]
    private string targetTag = "Hurtbox";
	[SerializeField]
	public SharedBool AttackOnCooldown = false;

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Running;
	}

    public override void OnTriggerEnter2D(Collider2D other)
    {
        CheckCollision(other);
    }

    public override void OnTriggerExit2D(Collider2D other)
    {
        CheckCollision(other);
    }

    private void CheckCollision(Collider2D other)
    {
        if (other.CompareTag(targetTag) && AttackOnCooldown.Value == false)
        {
            if (other.GetComponentInParent<HealthBase>() != null)
            {
                if (other.CompareTag(targetTag))
                {
                    AttackOnCooldown.Value = true;
                    other.GetComponentInParent<HealthBase>().ReceiveDamage(damageToInflict.Value);
                }
            }
        }
    }
}