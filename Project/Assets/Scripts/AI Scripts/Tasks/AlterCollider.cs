using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class AlterCollider : Action
{
    [SerializeField]
    private bool becomeTrigger = false;
	[SerializeField]
	private bool colliderEnabled = true;
	[SerializeField]
    private Collider2D thisCollider;

	public override void OnStart()
	{
		if (thisCollider == null)
		{
			thisCollider = gameObject.GetComponentInChildren<Collider2D>();
		}
	}

	public override TaskStatus OnUpdate()
	{
        thisCollider.isTrigger = becomeTrigger;
		thisCollider.enabled = colliderEnabled;
		return TaskStatus.Success;
	}
}