using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class InstantiateAndAssignPrefab : Action
{
    public SharedGameObject storageTarget;
    [SerializeField]
    private string nameAddon = "'s object";
    [SerializeField]
    private GameObject objectToSpawn;

    public override void OnStart()
    {
        storageTarget.Value = GameObject.Instantiate(objectToSpawn);
        storageTarget.Value.name = this.gameObject.name + nameAddon;

        storageTarget.Value.transform.parent = null;
        storageTarget.Value.transform.position = this.transform.position;
    }

    public override TaskStatus OnUpdate()
    {
        return TaskStatus.Success;
    }
}