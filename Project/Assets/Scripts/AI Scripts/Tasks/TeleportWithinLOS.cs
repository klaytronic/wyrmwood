using UnityEngine;
using System.Collections;

using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class TeleportWithinLOS : Action
{
    public SharedGameObject currentTarget;
    [SerializeField]
    private SharedGameObject teleportTether;

    [SerializeField]
    private float maximumDistance = 6f, 
        minimumDistance = 3f;
    [SerializeField]
    private LayerMask layersToCheck;

    public override void OnStart()
	{
        RollLocation();
	}

    private void RollLocation()
    {
        Vector2 rolledDirection = Random.insideUnitCircle;
        float length = minimumDistance + (maximumDistance - minimumDistance) * Random.value;
        Vector2 rolledLocation = (Vector2)currentTarget.Value.transform.position + rolledDirection.normalized * length;

        //// RaycastHit2D hit = Physics2D.Raycast(transform.position, currentTarget.Value.transform.position - transform.position, Vector2.Distance(transform.position, currentTarget.Value.transform.position), layersToCheck, Mathf.Infinity, Mathf.Infinity);
        //Vector2 direction = (Vector2)currentTarget.Value.transform.position - rolledLocation;
        //float distance = Vector2.Distance((Vector2)transform.position, (Vector2)currentTarget.Value.transform.position);

        //RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, distance, layersToCheck, Mathf.NegativeInfinity, Mathf.Infinity);

        //if (hit.collider == null)
        //{
        //    transform.position = rolledLocation;
        //    Debug.Log("Available");
        //}
        //else
        //{
        //    RollLocation();
        //}

        if (Physics2D.Linecast(rolledLocation, currentTarget.Value.transform.position, layersToCheck))
        {
            RollLocation();

        }
        else
        {
            transform.position = rolledLocation;
            teleportTether.Value.transform.position = rolledLocation;
        }

    }


	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}

    //IEnumerator IRollLocation()
    //{

    //    yield return new WaitForEndOfFrame();

    //}
    
}