using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class Dormant : Action
{
    [SerializeField]
    public SharedBool spawnHidden = false, activated = false;


    private Pathfinding.AIPath AIMovement;
    private SpriteRenderer spriteRender;
    private Collider2D collider;

    public override void OnAwake()
    {
        AIMovement = gameObject.GetComponentInChildren<Pathfinding.AIPath>();
        spriteRender = gameObject.GetComponentInChildren<SpriteRenderer>();
        collider = gameObject.GetComponentInChildren<Collider2D>();
    }

    public override void OnStart()
	{
        toggleComponents(false);
	}

	public override TaskStatus OnUpdate()
	{
        if (activated.Value)
        {
            toggleComponents(true);
            return TaskStatus.Success;
        } else
        {
            return TaskStatus.Running;
        }
    }


    private void toggleComponents(bool isEnabled)
    {
        AIMovement.canMove = isEnabled;

        if (spawnHidden.Value)
        {
            spriteRender.enabled = isEnabled;
            collider.enabled = isEnabled;
        }
    }
}