using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine.SceneManagement;

public class SetupAI : Action
{
    public SharedTransform destination;
    public SharedVector2 startingPosition;
    public SharedGameObject player;
    private GameObject objectToSpawn;


    public override void OnStart()
	{
        objectToSpawn = new GameObject(this.gameObject.name + " Destination");
        objectToSpawn.transform.parent = null;
        startingPosition.Value = (Vector2)transform.position;
        destination.Value = objectToSpawn.transform;
        player.Value = PlayerController.Instance.gameObject;
    }

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}

}