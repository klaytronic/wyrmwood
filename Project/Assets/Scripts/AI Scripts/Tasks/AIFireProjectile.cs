using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class AIFireProjectile : Action
{
    public SharedGameObject target;
    public SharedInt attackDamage;
    [SerializeField] public Transform projectileSpawnLocation;
    [SerializeField] private GameObject projectilePrefab;

    //[FMODUnity.EventRef]
    //public string attack = "event:/GhostAttack";

    //public FMOD.Studio.EventInstance ghostattackEv;


    public override void OnStart()
	{
        //ghostattackEv = FMODUnity.RuntimeManager.CreateInstance(attack);
        //ghostattackEv.start();

        if (projectileSpawnLocation == null)
        {
            projectileSpawnLocation = transform;
        }
        Vector2 direction = (Vector2)target.Value.transform.position - (Vector2)projectileSpawnLocation.position;
        direction = direction.normalized;
        float angleOfRotation = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        if (projectilePrefab != null)
        {
            EnemyProjectile spawnedProjectile = GameObject.Instantiate(projectilePrefab, projectileSpawnLocation.position, Quaternion.Euler(0, 0, angleOfRotation), null).GetComponentInChildren<EnemyProjectile>();
            spawnedProjectile.GetComponentInChildren<DamageOnContact>().damage = attackDamage.Value;
            spawnedProjectile.direction = direction;
            spawnedProjectile.damage = attackDamage.Value;
        } else
        {
            Debug.LogError("There is no projectile prefab tied to " + transform.name + "'s AIFireProjectile task. It cannot instantiate a projectile.");
        }

        
	}

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
}