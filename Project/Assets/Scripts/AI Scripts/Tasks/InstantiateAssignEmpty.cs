using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class InstantiateAssignEmpty : Action
{
    public SharedGameObject storageTarget;
    [SerializeField]
    private string nameAddon = "'s object";
    private GameObject objectToSpawn;

    public override void OnStart()
    {
        objectToSpawn = new GameObject(this.gameObject.name + nameAddon);
        objectToSpawn.transform.parent = null;
        objectToSpawn.transform.position = this.transform.position;
        storageTarget.Value = objectToSpawn;
    }

    public override TaskStatus OnUpdate()
    {
        return TaskStatus.Success;
    }
}