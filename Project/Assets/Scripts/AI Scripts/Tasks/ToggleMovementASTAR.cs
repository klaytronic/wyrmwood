using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class ToggleMovementASTAR : Action
{
    private Pathfinding.AIPath AIMovement;
    [SerializeField]
    private bool canMove = false;

    public override void OnAwake()
    {
        AIMovement = gameObject.GetComponentInChildren<Pathfinding.AIPath>();
    }

    public override void OnStart()
	{
        AIMovement.canMove = canMove;
    }

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
}