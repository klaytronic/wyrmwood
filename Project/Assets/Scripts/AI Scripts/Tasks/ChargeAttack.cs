using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using FMOD.Studio;

public class ChargeAttack : Action
{
    [Header("Charge Parameters")]
    [SerializeField]
    private float chargeDuration = 2;
    [SerializeField]
    private AnimationCurve chargeCurve;

    private Collider2D collider;

    //Lerp variables
    private float timePosition = 0;
    private Vector2 startingPosition;
    public SharedVector2 destination;


    private Pathfinding.AIPath AIMovement;

    private Transform spriteTransform;

    [FMODUnity.EventRef]
    public string attack = "event:/FlyAttack";

    public FMOD.Studio.EventInstance flyattackEv;

    public override void OnAwake()
    {
        flyattackEv = FMODUnity.RuntimeManager.CreateInstance(attack);

        AIMovement = gameObject.GetComponentInChildren<Pathfinding.AIPath>();
        collider = gameObject.GetComponentInChildren<Collider2D>();
        spriteTransform = transform.GetComponentInChildren<FlipGFXASTAR>().transform;
    }

    public override void OnStart()
	{
        flyattackEv.start();

        //Switch controls and collider
        AIMovement.canMove = false;
        collider.isTrigger = true;

        timePosition = 0;
        startingPosition = transform.position;
	}

	public override TaskStatus OnUpdate()
	{
        timePosition += Time.deltaTime;

        transform.position = Vector2.Lerp(startingPosition, destination.Value, chargeCurve.Evaluate(timePosition / chargeDuration));

        if ((Vector2)transform.position == destination.Value)
        {
            collider.isTrigger = false;
            AIMovement.canMove = true;
            return TaskStatus.Success;
        } else
        {
            return TaskStatus.Running;
        }
    }

    public override void OnLateUpdate()
    {
        //Debug.Log(destination);
        if (spriteTransform != null)
        {
            if (destination.Value.x <= transform.position.x)
            {
                spriteTransform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * -1f, transform.localScale.y, transform.localScale.z);
            }
            else if (destination.Value.x >= transform.position.x)
            {
                spriteTransform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
            }
        }
    }
}