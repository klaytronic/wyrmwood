using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class CalculateChargeDestination : Action
{
    [Header("Shared Variables")]
    public SharedGameObject target;
    public SharedTransform destinationMarker;
    public SharedVector2 destination;

    [Header("Charge Parameters")]
    [SerializeField]
    private float distancePastPlayer = 2, maximumChargeDistance = 8;
    private float stoppingOffset = 0.2f;

    [SerializeField]
    private LayerMask layersToCheck;
    private Vector2 directionToTarget;

    public override void OnStart()
    {
        directionToTarget = (Vector2)Vector3.Normalize(target.Value.transform.position - this.transform.position);
        RaycastHit2D wallCheckHit = Physics2D.Raycast(target.Value.transform.position, directionToTarget, distancePastPlayer, layersToCheck);

        if (wallCheckHit)
        {
            //Set the destination to the wall's location with a tiny offset
            destination.Value = -directionToTarget * stoppingOffset + wallCheckHit.point;
        }
        else
        {
            destination.Value = (Vector2)target.Value.transform.position + (directionToTarget * distancePastPlayer);
        }

        //Incase the charge is going too far
        float distanceToTravel = Vector2.Distance((Vector2)transform.position, destination.Value);
        if (distanceToTravel > maximumChargeDistance)
        {
            destination.Value = (Vector2)transform.position + (directionToTarget * maximumChargeDistance);
        }

        //FOR DEBUGGING
        //float distanceTraveling = Vector2.Distance((Vector2)transform.position, destination.Value);
        //Debug.Log("Distance from destination: " + distanceToTravel + ". Distance we actually charged: " + distanceTraveling);


        destinationMarker.Value.position = destination.Value;
    }

    public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
}