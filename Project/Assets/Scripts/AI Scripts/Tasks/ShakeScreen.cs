using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class ShakeScreen : Action
{

	[SerializeField] float time = 1;
	[SerializeField] Vector3 intensity;

	public override void OnStart()
	{
		iTweenExtensions.ShakePosition(Camera.main.gameObject, intensity, time, 0);
	}

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
}