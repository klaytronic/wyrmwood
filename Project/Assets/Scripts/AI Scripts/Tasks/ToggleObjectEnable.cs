using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class ToggleObjectEnable : Action
{
	[SerializeField]
	private GameObject target;
	[SerializeField]
	private bool enabled = true;

	public override void OnStart()
	{
		target.SetActive(enabled);
	}

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
}