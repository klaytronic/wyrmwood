using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class ReturnToSpawn : Action
{
    public SharedTransform destination;
    public SharedVector2 spawnPoint;

    private Pathfinding.AIPath AIMovement;

    public override void OnStart()
    {
        destination.Value.position = spawnPoint.Value;
        AIMovement = gameObject.GetComponentInChildren<Pathfinding.AIPath>();
        AIMovement.canMove = true;
    }

    public override TaskStatus OnUpdate()
	{
        if (Vector2.Distance((Vector2)transform.position, spawnPoint.Value) <= 0.5)
        {
            return TaskStatus.Success;

        }
        else
        {
            return TaskStatus.Running;
        }
    }
}