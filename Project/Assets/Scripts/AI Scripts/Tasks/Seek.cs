using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class Seek : Action
{
    public SharedGameObject currentTarget;
    public SharedTransform currentDestination;

    public SharedFloat speedModifier;

    private Pathfinding.AIPath AIMovement;
    private Pathfinding.AIDestinationSetter AINav;
    [SerializeField]
    private float baseSpeed;

    public override void OnAwake()
    {
        AIMovement = gameObject.GetComponentInChildren<Pathfinding.AIPath>();
        AINav = gameObject.GetComponentInChildren<Pathfinding.AIDestinationSetter>();

    }

    public override void OnStart()
	{
        AINav.target = currentDestination.Value;
	}

	public override TaskStatus OnUpdate()
	{
        //if (currentTarget.Value != null)
        //{
        //    if (evasiveMovement)
        //    {
        //        UpdateCurrentDestination(EvasiveMovement());
        //    } else
        //    {
        //        UpdateCurrentDestination(currentTarget.Value.transform.position);
        //    }
        //}

        AIMovement.maxSpeed = baseSpeed * speedModifier.Value;
        return TaskStatus.Running;
	}
}