﻿using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class CheckInRange : MonoBehaviour
{
    private BehaviorTree NpcTree;
    // Start is called before the first frame update
    void Start()
    {
        NpcTree = GetComponentInParent<BehaviorTree>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            NpcTree.SetVariableValue("PlayerInRange", true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            NpcTree.SetVariableValue("PlayerInRange", false);
        }
    }
}
