﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class FlipGFXASTAR : MonoBehaviour
{
    [SerializeField]
    private bool inverted = false;
    private Pathfinding.AIPath AIMovement;

    // Start is called before the first frame update
    void Start()
    {
        AIMovement = gameObject.GetComponentInParent<Pathfinding.AIPath>();
    }

    // Update is called once per frame
    void Update()
    {
        if (inverted == false)
        {
            if (AIMovement.desiredVelocity.x <= 0.01f)
            {
                transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * -1f, transform.localScale.y, transform.localScale.z);
            }
            else if (AIMovement.desiredVelocity.x >= 0.01f)
            {
                transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
            }
        } else
        {
            if (AIMovement.desiredVelocity.x >= 0.01f)
            {
                transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * -1f, transform.localScale.y, transform.localScale.z);
            }
            else if (AIMovement.desiredVelocity.x <= 0.01f)
            {
                transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
            }
        }
        
    }
}
