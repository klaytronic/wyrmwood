﻿using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class TriggerBTVarChanger : MonoBehaviour
{
    private BehaviorTree NpcTree;

    [Header("What info should be sent.")]

    [SerializeField]
    private bool sendCollisionGameobject;
    [SerializeField]
    private bool sendBoolean, sendExitBoolean;

    [Header("Tag the collisions are looking for.")]
    [SerializeField]
    private string tagToInteractWith;

    [Header("BT GameObject variable.")]
    [SerializeField]
    private string sharedGameObjectName;

    [Header("Boolean variables.")]
    [SerializeField]
    private string sharedBoolName;
    [SerializeField]
    private bool booleanOnEnter, booleanOnExit;

    // Start is called before the first frame update
    void Start()
    {
        NpcTree = GetComponentInParent<BehaviorTree>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(tagToInteractWith))
        {
            if (sendCollisionGameobject)
            {
                NpcTree.SetVariableValue(sharedGameObjectName, collision.gameObject);
            }
            if (sendBoolean)
            {
                NpcTree.SetVariableValue(sharedBoolName, booleanOnEnter);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(tagToInteractWith))
        {
            if (sendCollisionGameobject)
            {
                NpcTree.SetVariableValue(sharedGameObjectName, collision.gameObject);
            }
            if (sendExitBoolean)
            {
                NpcTree.SetVariableValue(sharedBoolName, booleanOnExit);
            }
        }
    }
}
