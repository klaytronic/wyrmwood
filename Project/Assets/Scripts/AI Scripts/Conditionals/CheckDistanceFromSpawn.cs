using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class CheckDistanceFromSpawn : Conditional
{
    [SerializeField]
    private float maxLeashRange;
    public SharedVector2 spawnPoint;

	public override TaskStatus OnUpdate()
	{
        if (Vector2.Distance(spawnPoint.Value, (Vector2)transform.position) >= maxLeashRange)
        {
            return TaskStatus.Success;
        } else
        {
            return TaskStatus.Running;
        }
    }
}