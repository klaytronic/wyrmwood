using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class WatchBool : Conditional
{
    public SharedBool watchedBoolean;
    [SerializeField]
    private bool targetBooleanState;

	public override TaskStatus OnUpdate()
	{
        if (watchedBoolean.Value == targetBooleanState)
        {

            return TaskStatus.Success;
        } else
        {
            return TaskStatus.Running;
        }
    }
}