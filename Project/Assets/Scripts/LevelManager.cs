﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class LevelManager : MonoBehaviour
{
    int sceneIndex;
    Image fade;

    CanvasGroup loadScreen;
    Slider loadBar;

    GameObject preview;
    Image cardArt;
    Image manaFrame;
    TMP_Text title, description;

    [SerializeField] float durationPadding = 2;

    bool loadingLevel = false;

    private void Start()
    {

        if (transform.parent == null)
            DontDestroyOnLoad(this.gameObject);
        loadScreen = Loadscreen.Instance.GetComponentInChildren<CanvasGroup>();
        loadBar = loadScreen.GetComponentInChildren<Slider>();

        loadScreen.alpha = 0;

        GameObject preview = loadScreen.transform.GetChild(1).gameObject;
        cardArt = preview.GetComponent<Image>();
        manaFrame = preview.transform.GetChild(0).GetComponent<Image>();
        title = preview.transform.GetChild(1).GetComponent<TMP_Text>();
        description = preview.transform.GetChild(2).GetComponent<TMP_Text>();
    }

    public void InitializeLoad(int sceneIndex_)
    {
        sceneIndex = sceneIndex_;

        loadingLevel = true;

        CardInfo info;

        if (ServiceLocator.Instance)
        {
            PlayerController.Instance.ToggleInput(false);
            ServiceLocator.Instance.cardSystem.ToggleInput(false);
            fade = ServiceLocator.Instance.fade;

            fade.enabled = true;
            fade.color = Color.clear;
            info = ServiceLocator.Instance.cardSystem.RandomCard();
        }
        else
        {
            info = GameObject.Find("cards").GetComponent<CardSystem>().RandomCard();
        }     

        cardArt.sprite = info.fullImage;
        if (ServiceLocator.Instance)
            manaFrame.sprite = ServiceLocator.Instance.cardSystem.manaFrames[info.manaCost - 1];
        else
            manaFrame.sprite = GameObject.Find("cards").GetComponent<CardSystem>().manaFrames[info.manaCost - 1];

        title.text = info.title;
        description.text = info.tipDescription;

        iTween.ValueTo(this.gameObject, iTween.Hash("from", 0,
                                                    "to", 1,
                                                    "time", 2,
                                                    "onupdate", "FadeSequence",
                                                    "onupdatetarget", this.gameObject,
                                                    "oncomplete", "LoadSceneAfter",
                                                    "oncompletetarget", this.gameObject));
    }

    void FadeSequence(float alpha)
    {
        if (loadingLevel)
            loadScreen.alpha = alpha;
    }

    void FinishFade()
    {
        if (loadingLevel)
        {
            loadingLevel = false;
            loadScreen.alpha = 0;
            loadBar.value = 0;
            if (GetComponent<ServiceLocator>() == null)
                Destroy(this.gameObject);
        }

    }

    void LoadSceneAfter()
    {
        if (fade)
            fade.color = Color.black;
        StartCoroutine(LoadSequence());
    }

    IEnumerator LoadSequence()
    {
        float timer = 0;
        while (timer < durationPadding)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while (!operation.isDone)
        {
            loadBar.value = Mathf.Clamp01(operation.progress / 0.9f);
            yield return null;
        }      

        loadBar.value = 1;

        fade = ServiceLocator.Instance.fade;
        fade.enabled = true;
        fade.color = Color.black;

        if (GetComponent<ServiceLocator>() == null)
        {
            iTween.ValueTo(this.gameObject, iTween.Hash("from", 1,
                            "to", 0,
                            "time", 2,
                            "onupdate", "FadeSequence",
                            "onupdatetarget", ServiceLocator.Instance.gameObject,
                            "oncomplete", "FinishFade",
                            "oncompletetarget", ServiceLocator.Instance.gameObject));
        }
        else
        {
            iTween.ValueTo(this.gameObject, iTween.Hash("from", 1,
                                "to", 0,
                                "time", 2,
                                "onupdate", "FadeSequence",
                                "onupdatetarget", this.gameObject,
                                "oncomplete", "FinishFade",
                                "oncompletetarget", this.gameObject));
        }

    }
}
