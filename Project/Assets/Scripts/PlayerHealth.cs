﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerHealth : HealthBase
{
	[SerializeField] Image bar;
	[SerializeField] TMP_Text text;
	[SerializeField] float barMoveTime;
	[SerializeField] float invulnAfterDamage;
	[SerializeField] float deathWait;
    SpriteRenderer sprite;

	PlayerController player;

	// Start is called before the first frame update
	void Start()
    {
		player = PlayerController.Instance;
		sprite = GetComponentInChildren<SpriteRenderer>();
		TookDamageEvent.AddListener(TakeHit);
		ReceivedHealingEvent.AddListener(Heal);
		bar = GameObject.Find("Health").transform.GetChild(1).GetComponent<Image>();
		text = GameObject.Find("Health").GetComponentInChildren<TMP_Text>();
		UpdateUI();
	}

	public void NewScene()
	{
		PlayerController.Instance.enabled = true;
		if (currentHealth <= 0)
		{
			ReceiveHealing(maxHealth);
			dead = false;
		}
	}



	void UpdateUI()
	{
		text.text = currentHealth.ToString() + "/" + maxHealth.ToString();
		iTween.ValueTo(gameObject, iTween.Hash("from", bar.fillAmount,
									   "to", currentHealth / maxHealth,
									   "time", barMoveTime,
									   "easetype", iTween.EaseType.easeOutQuint,
									   "onupdate", "BarSequence",
									   "onupdatetarget", gameObject,
									   "oncomplete", "BarComplete",
									   "oncompletetarget", gameObject,
									   "ignoretimescale", true));
	}

	void BarSequence(float value)
	{
		bar.fillAmount = value;
	}

	void BarComplete()
	{
		bar.fillAmount = currentHealth / maxHealth;
	}

	public void TakeHit(float damage)
	{	
		StartCoroutine(HitSequence(damage));
		UpdateUI();
	}

	public void Heal(float healing)
	{
		UpdateUI();
	}

	IEnumerator HitSequence(float damage)
	{
		ServiceLocator.Instance.hitstun.HitPlayer(damage);
		float timer = 0;

		player.hurtbox.enabled = false;
		while (timer < invulnAfterDamage)
		{
			timer += Time.deltaTime;
			yield return null;
		}
		if (!dead)
			player.hurtbox.enabled = true;

		yield break;
	}

	protected override void CheckIfDead()
	{
		if (!dead && currentHealth <= 0)
		{
			StartCoroutine(DeathSequence());
			DeathEvent.Invoke();
			dead = true;
		}		
	}

	IEnumerator DeathSequence()
	{
		player.ToggleInput(false);
		ServiceLocator.Instance.cardSystem.ToggleInput(false);

		player.anim.Play("Death");
		player.hurtbox.enabled = false;
		player.coll.enabled = false;
		player.StopDash();
		player.enabled = false;
		yield return new WaitForSeconds(deathWait);

		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

}
