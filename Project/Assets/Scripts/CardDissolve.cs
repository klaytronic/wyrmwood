﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CardDissolve : MonoBehaviour
{
    public Material dissolveMaterial;
    public bool canDissolve;
    public float dissolveSpeed;
    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        if (canDissolve)
        {
            dissolveMaterial.SetFloat("_Fade", iTween.FloatUpdate(dissolveMaterial.GetFloat("_Fade"), 0, dissolveSpeed));
        }
    }

    public void ResetFade()
    {
        dissolveMaterial.SetFloat("_Fade", 1);

    }
}
