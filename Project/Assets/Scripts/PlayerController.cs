﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class PlayerController : Singleton<PlayerController>
{
    public bool keyboardControls = false;
    
    [Header("Movement Metrics")]
	public float maxSpeed = 3f;
	public float slowedSpeed = 1.5f;

	[ReadOnly] public float currentSpeed;

	Vector2 moveInput, moveVector, moveLimit, aimInput;
	[HideInInspector] public Vector2 lookVector = Vector2.right;
	[System.NonSerialized] public Vector2 faceVector;
	[System.NonSerialized] public Quaternion inputRotation;
	float angle;

	[Header("Dash Metrics")]
	[SerializeField] float dashLength = 1f;
	[SerializeField] float dashTime = 0.1f;
	[SerializeField] float maxStamina = 3;
	[SerializeField] float staminaRechargeRate = 1f;
	[SerializeField] AnimationCurve dashEasing;
	public Collider2D hurtbox;
	[SerializeField] LayerMask wallLayer;
	[SerializeField] LayerMask dashThruLayer;
	[SerializeField] float barMoveTime;

	float stamina;
	Coroutine dash;

	[Header("Aim Metrics")]
	[SerializeField] float cursorAimSpeed;
	[SerializeField] bool aimTakesAnimPriority;

	[Header("UI")]
	[SerializeField] Transform staminaUI;
	[SerializeField] GameObject staminaIconPrefab;
	public GameObject reticleParent;
	Image staminaBar;

	[Header("Animation")]
	[SerializeField] float minRunAnimSpeed;
	[SerializeField] float maxRunAnimSpeed;

	[System.NonSerialized] public bool inputLock = true;
	[System.NonSerialized] public bool dashLock = false;

	[System.NonSerialized] public Collider2D coll;
	Rigidbody2D rb;
	public Animator anim;
	public Animator reflectionAnim;
	[HideInInspector] public GameObject reticle;
	Camera cam;
	[HideInInspector] public PlayerHealth health;

	bool staminaRecharging = false;
	bool slowed = false;
	bool cursor = false;
	bool willFall;
	bool inputAllow = true;
	bool dashing = false;
	bool rigged = false;
	public bool hitStunned;

	public GameObject dustParticles;

	public bool falling = true;
	public bool animLock = false;

    GameObject poof;

	public override void Awake()
	{
		base.Awake();

		coll = GetComponentInChildren<Collider2D>();
		anim = GetComponentInChildren<Animator>();
		rb = GetComponentInChildren<Rigidbody2D>();
		health = GetComponent<PlayerHealth>();

		staminaUI = GameObject.Find("Stamina").transform;
        poof = GameObject.Find("poof");
        if (poof)
            poof.SetActive(false);
	}

	public void NewScene()
	{
		rb.position = GameObject.Find("StartPosition").transform.position;

		ToggleInput(true);
		ServiceLocator.Instance.cardSystem.ToggleInput(true);
		if (!falling)
		{
			anim.Play("Idle");
		}
		hurtbox = transform.GetChild(1).GetComponent<Collider2D>();
		hurtbox.enabled = true;
		coll.enabled = true;
		cam = Camera.main;

		// establish current max speed
		currentSpeed = maxSpeed;
		stamina = maxStamina;
        staminaBar = GameObject.Find("Stamina").transform.GetChild(1).GetComponent<Image>();
        UpdateStaminaUI();
	}

	void Update()
	{
		if (hitStunned)
			return;

		if (!inputLock)
			GetInput();

		// if input is locked, then the player shouldn't be able to move,  aim, or dash
		// TODO ONCE WE HAVE ANIMATIONS: player should still animate

		if (Input.GetButtonDown("Dash") && falling)
        {
            falling = false;
            inputLock = false;
            ServiceLocator.Instance.cardSystem.castLock = false;
            if (reticle)
                reticle.SetActive(true);
            if (poof)
                poof.SetActive(true);
        }

		if (!inputLock || rigged)
			Aim();

        if (!falling && !animLock)
            Animate();

        if (falling)
        {
            ServiceLocator.Instance.cardSystem.castLock = true;
            if (reticle)
                reticle.SetActive(false);
        }

        if (inputLock)
            return;

        // if the dash button is pressed and there is enough stamina...
        if (Input.GetButtonDown("Dash") && stamina > 0 && inputAllow)
		{
			// TODO: allow the player to cancel dash into another dash. this if doesn't work currently.
			if (dash != null)
				StopCoroutine(dash);
			dash = StartCoroutine(Dash());

			// if stamina is not recharging, start recharging
			if (!staminaRecharging)
				StartCoroutine(RechargeStamina());
		}
	}

	private void FixedUpdate()
	{
		if (!inputLock || rigged)
			Move();		
	}

	void GetInput()
	{
		if (!inputAllow)
			return;
        moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        aimInput = new Vector3(-Input.GetAxis("Aim_Horz"), Input.GetAxis("Aim_Vert"));

        aimInput = falling ? Vector2.zero : aimInput;
	}

	private void Move()
	{
		// calculate movement
		moveVector = moveInput * currentSpeed;

		// apply velocity
		rb.velocity = moveVector;
	}

	public void StartRiggedMovement(Vector3 pos, Vector3 aim)
	{
        StopDash();
		StartCoroutine(RigMove(pos, aim));
	}

	IEnumerator RigMove(Vector3 pos, Vector3 aim)
	{
		rigged = true;

		while ((transform.position - pos).magnitude > 0.1f)
		{
			moveInput = (pos - transform.position).normalized;
			yield return null;
		}
		moveInput = Vector2.zero;
		transform.position = pos;
		aimInput = aim;
		rigged = false;

		yield break;
	}

	void Aim()
	{
		// if there is no input from the player
		if (aimInput == Vector2.zero)
		{
			// use the last directional input as the aim
			lookVector.x = -faceVector.x;
			lookVector.y = faceVector.y;

			// hide the reticle
			reticleParent.SetActive(false);
		}
		// otherwise set the aim from the input, show the reticle
		else
		{	
			lookVector = aimInput;
			reticleParent.SetActive(true);
		}

		// if aim style is rotational...
		if (!cursor)
		{
			// rotate the reticle to point in the aim direction
			inputRotation = Quaternion.Euler(Vector3.forward * (Mathf.Atan2(lookVector.x, lookVector.y) * Mathf.Rad2Deg));
			reticleParent.transform.rotation = inputRotation;
		}
		// otherwise move cursor style
		else
		{
			if (!reticle && reticleParent.transform.childCount > 0)
				reticle = reticleParent.transform.GetChild(0).gameObject;

			aimInput.x = -aimInput.x;

			if (reticle)
			{
				if (reticle.transform.localPosition == Vector3.zero)
					reticleParent.SetActive(false);
				else
					reticleParent.SetActive(true);

				Vector2 pos = reticle.transform.position;
				pos += aimInput * cursorAimSpeed * Time.deltaTime;
				pos.x = Mathf.Clamp(pos.x, cam.ScreenToWorldPoint(Vector3.zero).x, cam.ScreenToWorldPoint(Vector3.one * cam.pixelWidth).x);
				pos.y = Mathf.Clamp(pos.y, cam.ScreenToWorldPoint(Vector3.zero).y, cam.ScreenToWorldPoint(Vector3.one * cam.scaledPixelHeight).y);
				reticle.transform.position = pos;
			}
		}
	}

	void Animate()
	{
        // Decide how to animate:
        // aim's direction takes priority over movement's direction.

		if (dashing)
		{
			anim.Play("Dash");
			reflectionAnim.Play("Dash");
		}
        else if (aimTakesAnimPriority)
        {
            if (moveInput != Vector2.zero)
            {
                anim.Play("Move");
                reflectionAnim.Play("Move");            
            }
			else
			{
				anim.Play("Idle");
				reflectionAnim.Play("Idle");
				if (aimInput != Vector2.zero)
					faceVector = reticleParent.transform.GetChild(0).position - new Vector3(rb.position.x, rb.position.y);
				else if (moveInput != Vector2.zero)
					faceVector = moveInput;
			}
		}
		// otherwise movement's direction takes priority over aim's direction.
		else
		{
			aimInput.x = -aimInput.x;

			if (moveInput != Vector2.zero)
			{
				anim.Play("Move");
                reflectionAnim.Play("Move");
                faceVector = moveInput;
			}
			else
			{
				anim.Play("Idle");
                reflectionAnim.Play("Idle");

                if (aimInput != Vector2.zero)
				{
					if (reticleParent.transform.childCount > 0)
						faceVector = reticleParent.transform.GetChild(0).position - new Vector3(rb.position.x, rb.position.y);
				}

			}
		}

		// control variable run speed animation
		anim.SetFloat("Mag", Mathf.Clamp(moveVector.magnitude / maxSpeed, minRunAnimSpeed, maxRunAnimSpeed));
        reflectionAnim.SetFloat("Mag", Mathf.Clamp(moveVector.magnitude / maxSpeed, minRunAnimSpeed, maxRunAnimSpeed));

        // send animation direction to animator
        anim.SetFloat("AimX", faceVector.x);
		anim.SetFloat("AimY", faceVector.y);
        reflectionAnim.SetFloat("AimX", faceVector.x);
        reflectionAnim.SetFloat("AimY", faceVector.y);
    }

	// the coroutine that does all the dash sequencing.
	IEnumerator Dash()
	{
		dashing = true;
        dustParticles.GetComponent<ParticleSystem>().Play();
            //dashing = true;

		stamina--;
		UpdateStaminaUI();

		Vector2 lastValidPos = Vector2.zero;

		// lock casting ability, make invuln
		inputLock = true;
		ServiceLocator.Instance.cardSystem.castLock = true;
		hurtbox.enabled = false;

		// find the dash direction
		Vector2 dashDir = moveVector != Vector2.zero ? moveInput.normalized : faceVector.normalized;
        anim.SetFloat("AimX", dashDir.x);
        anim.SetFloat("AimY", dashDir.y);
        reflectionAnim.SetFloat("AimX", dashDir.x);
        reflectionAnim.SetFloat("AimY", dashDir.y);

        // find the start and end point
        Vector2 startPos = rb.position;
		Vector2 endPos = new Vector2(transform.position.x, transform.position.y) + dashDir * dashLength;

		// if there are any obstacles or pits in the path of this dash...
		RaycastHit2D[] results = new RaycastHit2D[10];
		if (Physics2D.RaycastNonAlloc(startPos, dashDir, results, (endPos - startPos).magnitude, dashThruLayer) > 0)
		{
			// check if there is a collider that is overlapping the dash's end position.
			Collider2D[] overlap = new Collider2D[1];
			Physics2D.OverlapPointNonAlloc(endPos, overlap, dashThruLayer);

			// turn off collisions for all other colliders in this path
			for (int index = 0; index < results.Length; ++index)
			{	
				if (results[index])
				{
					if (results[index].collider != overlap[0])
						Physics2D.IgnoreCollision(results[index].collider, coll, true);
				}
			}

			// if the overlapping collider is a pit, mark the player for falling and let the player pass into the pit
			if (overlap[0])
			{
				if (overlap[0].gameObject.layer == LayerMask.NameToLayer("Pit"))
				{
					willFall = true;
					lastValidPos = overlap[0].ClosestPoint(startPos);
					Physics2D.IgnoreCollision(overlap[0], coll, true);
				}
			}
		}

		// evaluate dash motion
		Vector2 lastPos = startPos;
		float time = 0;
		while (time < dashTime)
		{
			// find the next position to be at, apply speed needed to get there
			Vector2 nextPos = Vector2.Lerp(startPos, endPos, dashEasing.Evaluate(time / dashTime));

			if (Time.timeScale != 0)
				rb.velocity = (nextPos - lastPos) / Time.deltaTime;
			else
				rb.velocity = Vector2.zero;

			time += Time.deltaTime;
			lastPos = nextPos;
			yield return null;
		}

		// reenable the hurtbox, turn back on collisions with colliders that were turned off
		hurtbox.enabled = true;

		if (willFall)
		{
			willFall = false;
			GetComponent<PlayerHealth>().ReceiveDamage(1);
			transform.position = lastValidPos;			
		}
		
		foreach (RaycastHit2D info in results)
		{
			if (info.collider)
				Physics2D.IgnoreCollision(info.collider, coll, false);
		}

		// unlock input and casting
		inputLock = false;
		dashing = false;
		ServiceLocator.Instance.cardSystem.castLock = false;
       // dashing = false;
    }

	public void StopDash()
	{
		StopCoroutine(Dash());
		rb.velocity = Vector2.zero;
	}

	// called after dashing with a full bar of stamina. fills 1 stamina point and then calls itelf again until full.
	IEnumerator RechargeStamina()
	{
		// flip this boolean. Prevents this coroutine from being reset after dashing while the stamina is recharging.
		staminaRecharging = true;

		// wait the alloted time before adding stamina.
		float timer = 0;
		while (timer < staminaRechargeRate)
		{
			timer += Time.deltaTime;
			yield return 0;
		}
		stamina++;
		UpdateStaminaUI();

		// if there is still more stamina to recover, start a new coroutine instance.
		if (stamina < maxStamina)
			StartCoroutine(RechargeStamina());
		// otherwise flip the boolean that keeps this from rolling.
		else
			staminaRecharging = false;
	}

	void UpdateStaminaUI()
	{
		iTween.ValueTo(gameObject, iTween.Hash("from", staminaBar.fillAmount,
									   "to", stamina / maxStamina,
									   "time", barMoveTime,
									   "easetype", iTween.EaseType.easeOutQuint,
									   "onupdate", "StaminaBarSequence",
									   "onupdatetarget", gameObject,
									   "oncomplete", "StaminaBarComplete",
									   "oncompletetarget", gameObject,
									   "ignoretimescale", true));
	}

	void StaminaBarSequence(float value)
	{
		staminaBar.fillAmount = value;
	}

	void StaminaBarComplete()
	{
		staminaBar.fillAmount = stamina / maxStamina; 
	}

	// called by other scripts to change the max speed.
	public void ChangeSpeed(bool slow)
	{
		currentSpeed = slow ? slowedSpeed : maxSpeed;
	}

	public void ChangeAimType(bool newAim)
	{
		cursor = newAim;

		if (cursor)
			reticleParent.transform.rotation = Quaternion.identity;
	}

	public void ToggleInput(bool toggle)
	{
		inputAllow = toggle;

		if (!inputAllow)
		{
			moveInput = Vector2.zero;
			aimInput = Vector2.zero;
		}
	}
}
