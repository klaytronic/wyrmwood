﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class DestructibleObstacle : HealthBase
{
	[SerializeField] Vector2 shakeAmount;
	[SerializeField] float shakeTime;
	[SerializeField] float startingHealth;
	[SerializeField] float dropForce;
	ParticleSystem[] dusties;

	SpriteRenderer rend;

	[InfoBox("Order the Damage State sprites in order of Most Damaged to Least Damaged. Do not include currently set sprite.")]
	[PreviewField(100, ObjectFieldAlignment.Right)] [SerializeField] Sprite[] damageStates;

	private void Start()
	{
		rend = GetComponentInChildren<SpriteRenderer>();
		dusties = GetComponentsInChildren<ParticleSystem>();

		currentHealth = startingHealth;
		// change sprites
		int index = Mathf.FloorToInt((currentHealth / maxHealth) * (damageStates.Length + 1));
		rend.sprite = index > damageStates.Length || index <= 0 ? rend.sprite : damageStates[index - 1];
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.GetComponentInParent<DamageEnemy>())
		{
			DamageEnemy temp = collision.GetComponentInParent<DamageEnemy>();
			ReceiveDamage(temp.damage);
		}
	}

	public override void ReceiveDamage(float amount)
	{
		base.ReceiveDamage(amount);

		Debug.Log("yowch!");

		// change sprites
		int index = Mathf.FloorToInt((currentHealth / maxHealth) * (damageStates.Length + 1));
		rend.sprite = index > damageStates.Length || index <= 0 ? rend.sprite : damageStates[index - 1];

		// move & play dust bursts
		foreach (ParticleSystem burst in dusties)
		{
			Rect temp = rend.sprite.rect;
			burst.transform.localPosition = new Vector3((Random.value - 0.5f) * temp.width / 100,
														(Random.value - 0.5f) * temp.height / 100, 0);
			burst.transform.Rotate(0, 0, Random.Range(0, 360f));
			burst.Play();
		}

		gameObject.ShakePosition(shakeAmount, shakeTime, 0f);
	}

	protected override void CheckIfDead()
	{
		if (currentHealth <= 0)
		{
			Collider2D[] coll = GetComponents<Collider2D>();
            DeathEvent.Invoke();

            foreach (Collider2D collider in coll)
                collider.enabled = false;

			if (dusties.Length == 0)
				Destroy(this.gameObject);
			else
			{
				rend.enabled = false;

				if (Random.value < ServiceLocator.Instance.manaDropChance / 100)
				{
					GameObject mana = Instantiate(ServiceLocator.Instance.manaPickup, transform.position, Quaternion.identity);
					mana.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * dropForce, ForceMode2D.Impulse);
					mana.GetComponentInChildren<Animator>().Play("Burst");
				}
				

				foreach (ParticleSystem burst in dusties)
				{
					Rect temp = rend.sprite.rect;
					burst.transform.localPosition = new Vector3((Random.value - 0.5f) * temp.width / 100,
																(Random.value - 0.5f) * temp.height / 100, 0);
					burst.transform.Rotate(0, 0, Random.Range(0, 360f));
					burst.Play();
				}

				Destroy(this.gameObject, dusties[0].main.startLifetime.constantMax);
			}
		}
	}

	
}
