﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using UnityEditor;
using UnityEngine.UI;
using System.Linq;

public class CardAssetCreator :	OdinEditorWindow
{
	GameObject[] cardPresets;

	GameObject cardContainer;
	GameObject reticleContainer;
	GameObject elementContainer;
	Sprite mana1;
	Sprite mana2;
	Sprite mana3;

	[MenuItem("Tools/Card Asset Creator")]
   static void OpenWindow()
    {
		GetWindow<CardAssetCreator>().Show();
	}

	protected override void OnEnable()
	{
		base.OnEnable();

		cardContainer = CardAssetCreatorConfig.Instance.cardContainer;
		reticleContainer = CardAssetCreatorConfig.Instance.reticleContainer;
		elementContainer = CardAssetCreatorConfig.Instance.elementContainer;
		mana1 = CardAssetCreatorConfig.Instance.mana1;
		mana2 = CardAssetCreatorConfig.Instance.mana2;
		mana3 = CardAssetCreatorConfig.Instance.mana3;
		//cardEffectSequenceData = new CardEffectData[0];
		manaCost = manaCost == 0 ? 1 : manaCost;
	}

	[Button(ButtonSizes.Medium), GUIColor(0, 1, 0)]
	void SaveCardAsset()
	{
        // warn user
		if (!EditorUtility.DisplayDialog("HOL UP", "This will create a new card asset. It will also overwrite any existing assets named after this card. Are you prepared??", "Yes", "No"))
			return;

        // instantiate an empty card in scene
		GameObject assetInstance = Instantiate(cardContainer);

        // save this card as a prefab in the appropriate spot
		GameObject asset = PrefabUtility.SaveAsPrefabAsset(assetInstance, "Assets/Prefabs/Cards/Resources/card_" + cardName + ".prefab");

        // get the card's properties
		CardProperties properties = asset.GetComponent<CardProperties>();

        // set playability
		properties.playable = playable;

        // check if there is an existing data folder, or make one
		if (!AssetDatabase.IsValidFolder("Assets/Prefabs/Cards/Data/card_" + cardName))
			AssetDatabase.CreateFolder("Assets/Prefabs/Cards/Data", "card_" + cardName);

        // create an instance of card info, grab data
		CardInfo info = CreateInstance<CardInfo>();
		info.title = cardName;
		info.description = description;
		info.manaCost = manaCost;
        info.cursorAim = cursorAim;
        info.fullImage = cardArt;
        info.thumb = thumb;
        switch (manaCost)
		{
			case 1:
				asset.transform.GetChild(1).GetComponent<Image>().sprite = mana1;
				break;
			case 2:
				asset.transform.GetChild(1).GetComponent<Image>().sprite = mana2;
				break;
			case 3:
				asset.transform.GetChild(1).GetComponent<Image>().sprite = mana3;
				break;
			default:
				EditorUtility.DisplayDialog("YOU JERK!!!", "You made the mana cost of this card beyond the possible values!!", "I'm sorry :(", "I like it like that >:)");
				break;
		}
		asset.transform.GetChild(0).GetComponent<Image>().sprite = info.thumb;
       
		GameObject reticleAssetInstance;
		GameObject reticleAsset;

        // if there is a specified reticle, save it
        if (reticle != null)
        {
            // create instance, save as prefab, delete instance
            reticleAssetInstance = Instantiate(reticle);              
            reticleAsset = PrefabUtility.SaveAsPrefabAsset(reticleAssetInstance, "Assets/Prefabs/Cards/Data/card_" + cardName + "/card_" + cardName + "_reticle.prefab");              
            PrefabUtility.SavePrefabAsset(reticleAsset);

            info.reticle = reticleAsset;
            DestroyImmediate(reticleAssetInstance);
        }
        // if there is no specified reticle...
        else
        {
            // look for one already saved and hook the first one up
            string[] temp = AssetDatabase.FindAssets("reticle", new[] { "Assets/Prefabs/Cards/Data/card_" + cardName});
            if (temp.Length > 0)
            {
                GameObject tempRet = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Cards/Data/card_" + cardName + "/" + temp[0], typeof(GameObject));
                info.reticle = tempRet;
                reticle = tempRet;
            }
            // if there is no saved reticle, make a fresh one and hook it up
            else
            {
                reticleAssetInstance = Instantiate(reticleContainer);
                reticleAsset = PrefabUtility.SaveAsPrefabAsset(reticleAssetInstance, "Assets/Prefabs/Cards/Data/card_" + cardName + "/card_" + cardName + "_reticle.prefab");
                PrefabUtility.SavePrefabAsset(reticleAsset);
                info.reticle = reticleAsset;
                DestroyImmediate(reticleAssetInstance);
            }
        }

        // save the cardinfo instance as an asset, hook it up to the card
		AssetDatabase.CreateAsset(info, "Assets/Prefabs/Cards/Data/card_" + cardName + "/card_" + cardName + ".asset");
		properties.info = info;

		int numOfElements = 0;

        // check every card effect component specified
		for (int i = 0; i < cardEffectSequenceData.Length; ++i)
		{
            // if this is a spawn element card effect...
			if (cardEffectSequenceData[i] is SpawnElementData)
			{
                // add the component to the card instance, cast the data to the correct class
				SpawnElement tempComponent = asset.AddComponent<SpawnElement>();
				SpawnElementData tempData = (SpawnElementData)cardEffectSequenceData[i];

				GameObject tempElementInstance;
				string elemName;

                // if there is a specified element, save that
				if (tempData.element != null)
				{
					tempElementInstance = Instantiate(tempData.element);
                    elemName = "Assets/Prefabs/Cards/Data/card_" + cardName + "/" + tempData.element.name + ".prefab";
				}
                // otherwise make an empty one and connect that
				else
				{
					tempElementInstance = Instantiate(elementContainer);
					elemName = "Assets/Prefabs/Cards/Data/card_" + cardName + "/card_" + cardName + "_element" + numOfElements.ToString() + ".prefab";
					numOfElements++;
				}

				GameObject tempElement;

				tempElement = PrefabUtility.SaveAsPrefabAsset(tempElementInstance, elemName);						

                if (tempData.element == null)
                {
                    if (tempData.projOrElem)
                        DestroyImmediate(tempElement.GetComponentInChildren<DamageEnemy>(), true);
                    else
                    {
                        DamageEnemy tempDamage = tempElement.GetComponentInChildren<DamageEnemy>();
                        tempDamage.damage = tempData.damage;
                        tempDamage.melee = tempData.melee;
                        tempDamage.cleave = tempData.cleave;
                    }
                }

				tempComponent.condition = tempData.activationCondition;
				tempComponent.startLag = tempData.startLag;
				tempComponent.endLag = tempData.endLag;
				tempComponent.waitToFinish = tempData.waitToFinish;

				PrefabUtility.SavePrefabAsset(tempElement);

                tempComponent.element = tempElement;

                DestroyImmediate(tempElementInstance);

				tempComponent.directional = tempData.directional;
				tempComponent.spawnAtCursor = tempData.spawnAtCursor;
			}
			else if (cardEffectSequenceData[i] is MovePlayerData)
			{
				MovePlayer tempComponent = asset.AddComponent<MovePlayer>();
				MovePlayerData tempData = (MovePlayerData)cardEffectSequenceData[i];
				tempComponent.condition = tempData.activationCondition;
				tempComponent.startLag = tempData.startLag;
				tempComponent.endLag = tempData.endLag;
				tempComponent.rotateRelativeToPlayer = tempData.rotateRelativeToPlayer;
				tempComponent.invulnerability = tempData.invulnerability;
				tempComponent.target = tempData.target;
				tempComponent.moveTime = tempData.moveTime;
				tempComponent.easing = tempData.easing;
				tempComponent.waitToFinish = tempData.waitToFinish;
				tempComponent.dashAnim = tempData.dashAnim;
			}
		}
		PrefabUtility.SavePrefabAsset(asset);
		DestroyImmediate(assetInstance);
	}

	[HorizontalGroup("Split")]
	[VerticalGroup("Split/Left")]

	[VerticalGroup("Split/Left/General")] [Title("General Properties")] [SerializeField] string cardName;
	[VerticalGroup("Split/Left/General")] [SerializeField] [MultiLineProperty(6)] string description;

	bool playable;
	string playButtonName;	
	[PropertyTooltip("Press to toggle playablility.")] [Button("$playButtonName", ButtonSizes.Medium), GUIColor("PlayColor")] [ButtonGroup("Split/Left/General/Buttons")]
	void TogglePlay()
	{
		playable = !playable;

	}
	Color PlayColor()
	{
		playButtonName = playable ? "Card Playable" : "Card Unplayable";
		return playable ? Color.cyan : Color.red;
	}

	bool cursorAim;
	string aimButtonName;
	[PropertyTooltip("Press to toggle aim style.\nCursor: this card has a free-motion cursor that can be moved anywhere on the screen.\nDirectional: this card can only be aimed via direction.")] [Button("$aimButtonName", ButtonSizes.Medium), GUIColor("AimColor")] [ButtonGroup("Split/Left/General/Buttons")]
	void ToggleAim()
	{
		cursorAim = !cursorAim;
	}
	Color AimColor()
	{
		aimButtonName = cursorAim ? "Aim Style: Cursor" : "Aim Style: Directional";
		return cursorAim ? Color.magenta : Color.green;
	}

	[VerticalGroup("Split/Left/General")] [EnableIf("playable")] [SerializeField] [ValueDropdown("ManaCosts")] [Tooltip("How expensive this card is to play.")] int manaCost;

	[VerticalGroup("Split/Left/Effects")]
	[Title("Card Effects")]
	[VerticalGroup("Split/Left/Effects")] [SerializeField] [Tooltip("You can select an existing card to build off of.")] [OnValueChanged("PresetChangeWarning")] [ValueDropdown("GetPresets")] int cardPreset;
	[VerticalGroup("Split/Left/Effects")] [SerializeField] [ValueDropdown("GetEffects")] CardEffectData[] cardEffectSequenceData;
	int oldPreset;

	int[] ManaCosts = new int[] { 1, 2, 3 };
 
	[VerticalGroup("Split/Right")][Title("Visuals")]
	[VerticalGroup("Split/Right")] [SerializeField] [PreviewField(200, ObjectFieldAlignment.Right)] Sprite cardArt;
	[VerticalGroup("Split/Right")] [SerializeField] [PreviewField(100, ObjectFieldAlignment.Right)] Sprite thumb;
	[VerticalGroup("Split/Right")] [SerializeField] GameObject reticle;

	IList<ValueDropdownItem<int>> GetPresets()
	{
		Object[] temp = Resources.LoadAll("", typeof(GameObject));
		cardPresets = new GameObject[temp.Length];
		for (int index = 0; index < temp.Length; ++index)
			cardPresets[index] = (GameObject)temp[index];

		ValueDropdownList<int> list = new ValueDropdownList<int>();

		for (int i = 0; i < cardPresets.Length; ++i)
			list.Add(cardPresets[i].name, i);

		return list;
	}

	IEnumerable GetEffects = new ValueDropdownList<CardEffectData>()
	{
		{"SpawnElement", new SpawnElementData()},
		{"MovePlayer", new MovePlayerData()},
	};

	void PresetChangeWarning()
	{
		if (EditorUtility.DisplayDialog("HOL UP", "This will reset all your card effect changes. Continue?", "Heck yeah!", "Heck no!"))
		{
			oldPreset = cardPreset;
			PresetAutoFill(cardPreset);
			
		}
		else
		{
			cardPreset = oldPreset;
		}
	}

	void PresetAutoFill(int preset)
	{
		GameObject card = cardPresets[preset];

		CardProperties prop = card.GetComponent<CardProperties>();

		CardInfo info = prop.info;

		CardEffect[] effects = card.GetComponentsInChildren<CardEffect>();

		cardEffectSequenceData = new CardEffectData[effects.Length];

		cardName = info.title;
		description = info.description;
		manaCost = info.manaCost;
		cursorAim = info.cursorAim;
		cardArt = info.fullImage;
		thumb = info.thumb;
		playable = prop.playable;
        reticle = info.reticle;

		for (int i = 0; i < effects.Length; ++i)
		{
			if (effects[i] is SpawnElement)
			{
				cardEffectSequenceData[i] = new SpawnElementData();
				SpawnElementData temp = (SpawnElementData)cardEffectSequenceData[i];
				temp.PullData((SpawnElement)effects[i]);
			}
			else if (effects[i] is MovePlayer)
			{
				cardEffectSequenceData[i] = new MovePlayerData();
				MovePlayerData temp = (MovePlayerData)cardEffectSequenceData[i];
				temp.PullData((MovePlayer)effects[i]);
			}
		}
	}

}
