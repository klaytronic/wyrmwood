﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.Utilities;

[GlobalConfig("Assets/Editor/CardAssetCreator/")]
public class CardAssetCreatorConfig : GlobalConfig<CardAssetCreatorConfig>
{
	public GameObject cardContainer;
	public GameObject reticleContainer;
	public GameObject elementContainer;
	public Sprite mana1;
	public Sprite mana2;
	public Sprite mana3;
}
