﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;
using System.Security.Cryptography;

public class CardSystem : MonoBehaviour
{
	[TabGroup("Card Data")]
	[ReadOnly] public List<GameObject> availCards;
    [TabGroup("Card Data")] public Sprite[] manaFrames;

	[Header("Card Piles")] [ValueDropdown("GetCardList")] public List<int> deckBuild;
	[ReadOnly] public List<GameObject> liveDeck;
	[ReadOnly] public List<GameObject> hand;
	[ReadOnly] public List<GameObject> discardPile;

	[TabGroup("Hand Metrics")] [SerializeField] float mulliganHoldLength = 1f;
	[TabGroup("Hand Metrics")] [SerializeField] int handSize = 5;
	[TabGroup("Hand Metrics")] [SerializeField] float refillDeckDuration;
	[TabGroup("Hand Metrics")] [SerializeField] float drawTime;
	[TabGroup("Hand Metrics")] public int maxMana = 3;
	[TabGroup("Hand Metrics")] [SerializeField] float manaRechargeRate;

	[TabGroup("UI")] [SceneObjectsOnly] public Image refillMeter;
    [TabGroup("UI")] public GameObject movingCard;
    [TabGroup("UI")] public float selectedCardSize = 0.75f;
	[TabGroup("UI")] public float unselectedCardSize = 1f;
	[TabGroup("UI")] public float lerpTime = 0.25f;
	[TabGroup("UI")] public float cardSpacing = 1;
	[TabGroup("UI")] public float pileSize = 0.3f;
    [TabGroup("UI")] public float pileAddScale = 0.4f;
    [TabGroup("UI")] public float pileRemoveScale = 0.2f;
    [TabGroup("UI")] public Sprite NoArt;

    [TabGroup("Testing")] [ReadOnly] public bool castLock;
	[TabGroup("Testing")] [ReadOnly] public bool selectLock;
	[TabGroup("Testing")] [ShowInInspector] [ReadOnly] bool manaRecharging;
	[TabGroup("Testing")] [ReadOnly] public int mana;

	Transform handTransform;
	[HideInInspector] public Transform deckTransform;
	[HideInInspector] public Transform discardTransform;
	Transform manaTransform;
	Transform cardUI;
	Transform openUI;
	Transform closeUI;
    Transform hud;
	float openY;
	bool visible = true;
	TMP_Text deckText;
	TMP_Text discardText;
	Image[] manaOrbs;
	Animator[] manaOrbBlinks;

	Transform reticle;
	GameObject cardPrefab, physicalCard;
	int handIndex, oldIndex;
	bool castPressed;
	bool inputAllow = true;
	bool mulliganPressed;

    bool deckRefilling;

	float pressTime;
	float manaPercent;

	CardInfo oldInfo;

	PlayerController player;

    public UISoundManager UISound;

    // this bad boy can fit so much <<BUTTON THAT REFRESHES CARD LIST>> in it.
    [Button("Refresh Card List")]
	[TabGroup("Card Data")]
	[PropertyOrder(1)]
	public void RefreshCardList()
	{
		Object[] temp = Resources.LoadAll("", typeof(GameObject));

		for (int index = 0; index < temp.Length; ++index)
		{
			if (!availCards.Contains((GameObject)temp[index]))
				availCards.Add((GameObject)temp[index]);
		}

		for (int index = 0; index < availCards.Count; ++index)
		{
			if (availCards[index] == null)
			{
				availCards.RemoveAt(index);
				for (int i = 0; i < deckBuild.Count; ++i)
				{
					if (deckBuild[i] > index)
						deckBuild[i]--;
				}
			}
			else
			{
				for (int i = 0; i < availCards.Count; ++i)
				{
					if (availCards[i] == availCards[index] && i != index)
					{
						availCards.RemoveAt(index);
						for (int j = 0; j < deckBuild.Count; ++j)
						{
							if (deckBuild[j] > index)
								deckBuild[j]--;
						}
					}
				}
			}
		}
		CardDB.Instance.cards = availCards;
	}

    public CardInfo RandomCard()
    {
        return availCards[Random.Range(0, availCards.Count)].GetComponent<CardProperties>().info;
    }

    public void Awake()
	{
		if (deckBuild.Count > 0)
			ToggleInput(true);

		hud = transform.GetChild(0);

		if (!hud)
			throw new System.Exception("There is not HUDCanvas");

		cardUI = hud.GetChild(2);

		handTransform = cardUI.GetChild(0);

		manaTransform = cardUI.GetChild(2);
		deckTransform = cardUI.GetChild(3);
		discardTransform = cardUI.GetChild(4);
		deckText = deckTransform.GetComponentInChildren<TMP_Text>();
		discardText = discardTransform.GetComponentInChildren<TMP_Text>();
		openUI = hud.GetChild(5);
		closeUI = hud.GetChild(6);

        openY = handTransform.position.y;
    }

	public void NewScene()
	{
		StartCoroutine(Initialize());
	}

    // this bad boy can fit so much <<CALLED ON SCENE RELOAD, RE-FINDS REFERENCES AND RESETS ALL CARD PILES>> in it.
    public IEnumerator Initialize()
	{
        // find references
		player = PlayerController.Instance;
		refillMeter = PlayerController.Instance.transform.GetComponentInChildren<Image>();
		reticle = PlayerController.Instance.reticleParent.transform;		
        manaOrbs = GameObject.Find("Mana").GetComponentsInChildren<Image>();
        manaOrbBlinks = GameObject.Find("ManaBlink").GetComponentsInChildren<Animator>();

        // refresh the deck with cards from the deck build
        liveDeck = new List<GameObject>();
        foreach (int index in deckBuild)
			liveDeck.Add(availCards[index]);

        // refresh mana
		SpendMana(mana);
		gainMana(maxMana);

        // shuffle the deck
		yield return StartCoroutine(Shuffle());

		// empty the hand
		if (handTransform.childCount != 0)
		{
			yield return StartCoroutine(EmptyHand());
			while (handTransform.childCount != 0)
				yield return 0;
		}

        // clear discard pile
        discardPile = new List<GameObject>();

        // fill the hand
        StartCoroutine(FillHand(drawTime));
		yield break;
	}

    // this bad boy can fit so much <<SHUFFLES THE DECK>> in it.
    public IEnumerator Shuffle()
	{
		RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
		int n = liveDeck.Count;
		// todo: add a shuffle time and animation
		while (n > 1)
		{
			byte[] box = new byte[1];
			do provider.GetBytes(box);
			while (!(box[0] < n * (byte.MaxValue / n)));
			int k = (box[0] % n);
			n--;
			GameObject value = liveDeck[k];
			liveDeck[k] = liveDeck[n];
			liveDeck[n] = value;
		}
		Debug.Log("deck shuffled!");
		yield break;
	}

    // this bad boy can fit so much <<PUBLIC FUNCTION ALLOWING OTHER SCRIPTS TO TOP UP THE HAND>> in it.
    public void RequestFillHand()
	{
		StartCoroutine(FillHand(drawTime));
	}

    // this bad boy can fit so much <<FILLS THE HAND WITH CARDS UP TO MAX HAND SIZE>> in it.
    public IEnumerator FillHand(float delay)
	{
		// no card actions allowed.
		castLock = true;
		selectLock = true;
		float timer;

		// draw cards back up to full hand size
		int handDiff = handSize - hand.Count;
		for (int index = 0; index < handDiff; ++index)
		{
			if (liveDeck.Count == 0 && discardPile.Count == 0)
				break;

			StartCoroutine(DrawCard(-1));


			timer = 0;
			while (timer < delay / handDiff)
			{
				timer += Time.deltaTime;
				yield return 0;
			}

            while (deckRefilling)
                yield return null;
		}

        // wait until the hand is drawn up, or both piles are empty
		while (handTransform.childCount < handSize)
        {
            if (liveDeck.Count == 0 && discardPile.Count == 0 && handTransform.childCount == deckBuild.Count)
                break;
            yield return 0;
        }

		// set the selection to the middle, moving right if that card is unplayable
		SetSelection(handIndex);
		Debug.Log("hand filled");

        // unlock card actions
		castLock = false;
		selectLock = false;
	}

    // this bad boy can fit so much <<EMPTIES THE HAND>> in it.
    public IEnumerator EmptyHand()
	{
        // if the physical hand is empty then this is easy lol
		if (handTransform.childCount == 0)
		{
			hand = new List<GameObject>();
			yield break;
		}

        // send each card to the discard pile, activating any discard effects on each card
		for (int index = hand.Count - 1; index >= 0; --index)
		{
			CardProperties temp = handTransform.GetChild(index).GetComponent<CardProperties>();
			StartCoroutine(temp.ActivateCondition(Condition.Discard));

			while (!temp.completed)
				yield return null;

			StartCoroutine(SendToDiscard(index, handTransform.GetChild(index).gameObject));
		}

        while (handTransform.childCount != 0)
            yield return null;

        // clear the hand
        hand = new List<GameObject>();
	}

    // this bad boy can fit so much <<SHUFFLES AND REFILLS THE DECK FROM THE DISCARD PILE>> in it.
	public IEnumerator RefillDeck()
	{
		selectLock = true;
        castLock = true;
        deckRefilling = true;

        int count = discardPile.Count;

        float timing = refillDeckDuration / count;

        for (int i = count - 1; i >= 0; --i)
        {
            Debug.Log(i);
            StartCoroutine(DiscardToDeck(discardPile[i]));
            discardPile.RemoveAt(i);
            
            float timer = 0;
            while (timer < timing)
            {
                timer += Time.deltaTime;
                yield return null;
            }
        }

        while (liveDeck.Count < count)
            yield return null;

        yield return StartCoroutine(Shuffle());

		discardPile = new List<GameObject>();
        deckRefilling = false;
		yield break;
	}

    IEnumerator DiscardToDeck(GameObject card)
    {
        GameObject mini = MoveCard(discardTransform.position, deckTransform.position);

        iTween.ValueTo(this.gameObject, iTween.Hash("from", pileRemoveScale,
                                            "to", pileSize,
                                            "time", 0.25f,
                                            "onupdate", "DiscardPileScaleUpdate"
                                            ));

        while (mini)
            yield return null;

        iTween.ValueTo(this.gameObject, iTween.Hash("from", pileAddScale,
                                    "to", pileSize,
                                    "time", 0.25f,
                                    "onupdate", "DrawPileScaleUpdate"
                                    ));

        liveDeck.Add(card);
    }

    // this bad boy can fit so much <<SENDS A CARD IN THE HAND INTO THE DISCARD PILE>> in it.
    public IEnumerator SendToDiscard(int cardIndex, GameObject liveCard)
	{       
        GameObject cardMini = MoveCard(liveCard.transform.position, discardTransform.position);
        liveCard.transform.position = Vector3.one * 10000;

        while (cardMini)
            yield return null;

		discardPile.Add(hand[cardIndex]);

        iTween.ValueTo(this.gameObject, iTween.Hash("from", pileAddScale,
                                            "to", pileSize,
                                            "time", 0.25f,
                                            "onupdate", "DiscardPileScaleUpdate"));

        hand.RemoveAt(cardIndex);
		hand.TrimExcess();

        Destroy(liveCard);

        yield break;
	}

    // this bad boy can fit so much <<REORGANIZES THE POSITION OF ALL CARDS IN THE HAND>> in it.
    //TODO: this should scale with screen size, on tiny screens the cards are way too far from each other.
    public IEnumerator ReorganizeHand()
	{
        float offset = (hand.Count % 2 == 0) ? 0.5f : 0;

        for (int index = 0; index < handTransform.childCount; ++index)
			StartCoroutine(LerpCard(handTransform.GetChild(index), handTransform.position + (Vector3.right * (index - (hand.Count / 2) + offset) * (cardSpacing * hud.GetComponent<Canvas>().scaleFactor)), false));
			
		yield break;
	}

    // this bad boy can fit so much <<DRAWS A SINGLE CARD INTO THE HAND>> in it.
    public IEnumerator DrawCard(int slot)
	{
        // if no cards to draw, refill the deck first.
        
		if (liveDeck.Count == 0)
			yield return RefillDeck();

        while (deckRefilling)
            yield return null;

        // pull a card out of the live deck
		GameObject drawn = liveDeck[liveDeck.Count - 1];
		liveDeck.RemoveAt(liveDeck.Count - 1);

        //DRAW CARD SOUND
        UISound.DrawCard();

        float offset = ((hand.Count + 1) % 2 == 0) ? 0.5f : 0;
        GameObject cardMini;
        Vector3 cardEndPos;

        // if the slot int is negative then just add it, otherwise put it in into the proper slot, use card effect
        if (slot < 0)
        {
            hand.Add(drawn);
            cardEndPos = handTransform.position + (Vector3.right * ((hand.Count - 1) - (hand.Count / 2) + offset) * cardSpacing);
        }
        else
        {
            hand.Insert(slot, drawn);
            cardEndPos = handTransform.position + (Vector3.right * (slot - (hand.Count / 2) + offset) * cardSpacing);
        }

        iTween.ValueTo(this.gameObject, iTween.Hash("from", pileRemoveScale,
                                                    "to", pileSize,
                                                    "time", 0.25f,
                                                    "onupdate", "DrawPileScaleUpdate"
                                                    ));

        cardMini = MoveCard(deckTransform.position, cardEndPos);

        // wait for mini card to end
        while (cardMini)
            yield return null;

        // instantiate the physical card
        GameObject temp = Instantiate(drawn, cardEndPos, Quaternion.identity, handTransform);
		temp.transform.localScale = Vector3.one * unselectedCardSize;

        // refresh card's playability
		CheckManaExpenses(temp.GetComponent<CardProperties>());
		if (slot >= 0)
			temp.transform.SetSiblingIndex(slot);

        // set selected card
		SetSelection(handIndex);

        // reorganize hand
		yield return StartCoroutine(ReorganizeHand());
	}

    // this bad boy can fit so much <<PERFORMS ALL THE ACTIONS TO DO A MULLIGAN>> in it.
    public IEnumerator Mulligan()
	{
        castLock = true;
        selectLock = true;
        // if theres stuff in the hand, empty it
		if (handTransform.childCount != 0)
		{
			yield return StartCoroutine(EmptyHand());
			while (handTransform.childCount != 0)
				yield return 0;
		}

        // now fill
		StartCoroutine(FillHand(drawTime));
		yield break;
	}

    public GameObject MoveCard(Vector3 card, Vector2 target)
    {
        GameObject temp = Instantiate(movingCard, card, Quaternion.identity, hud);
        temp.GetComponent<CardMover>().MoveTo(target);
        return temp;
    }

    // this bad boy can fit so much <<LERPS A SINGLE CARD TO A SPECIFIED SPOT, DESTROYING AT THE END IF NECESSARY>> in it.
    // TODO: this doesn't look that good anymore, should find a way to path more interestingly
    public IEnumerator LerpCard(Transform card, Vector3 target, bool destroyAtEnd)
	{
		float timer = 0;
		while (timer < lerpTime)
		{
			if (!card)
				yield break;

			card.position = Vector2.Lerp(card.position, target, timer / lerpTime);
			timer += Time.deltaTime;
			yield return null;
		}
		card.position = target;

		if (destroyAtEnd)
			Destroy(card.gameObject);
 
		yield break;
	}

    // this bad boy can fit so much <<SETS THE SELECTED CARD IN THE HAND, HANDLES NEW RETICLE INFORMATION>> in it.
    public void SetSelection(int index)
	{
		// if hand is empty, return
		if (handTransform.childCount == 0)
		{
			cardPrefab = null;
			physicalCard = null;

			// get rid of all reticles
			foreach (Transform temp in reticle)
				Destroy(temp.gameObject);

			return;
		}			

		// shrink down all cards
		foreach (Transform card in handTransform)
			card.localScale = Vector3.one * unselectedCardSize;

		// clamp index selector to the hand count.
		index = index > hand.Count - 1 ? hand.Count - 1 : index;
		oldIndex = oldIndex > hand.Count - 1 ? hand.Count - 1 : oldIndex;

		// if the old index selector is inside the hand, unselect the card it was
		if (oldIndex < handTransform.childCount && oldIndex >= 0 )
			handTransform.GetChild(oldIndex).localScale = Vector3.one * unselectedCardSize;

		// save the currectly selected card's info and the physical card
		cardPrefab = hand[index];
        if (handTransform.childCount - 1 >= index)
		    physicalCard = handTransform.GetChild(index).gameObject;

		CardInfo info = cardPrefab.GetComponent<CardProperties>().info;

		player.ChangeAimType(info.cursorAim);

		// if the selected card is playable...
		if (info.manaCost <= mana)
		{
			// if the newly selected card is different from the old selected card, swap projectiles
			if (info != oldInfo)
			{
				// destroy all reticles and add the proper one
				foreach (Transform child in reticle)
					Destroy(child.gameObject);
				PlayerController.Instance.reticle = Instantiate(info.reticle, reticle);
				oldInfo = info;
			}
		}
        // if the selected card is not playable, get rid of all reticles
		else
		{
			foreach (Transform child in reticle)
				Destroy(child.gameObject);
			oldInfo = null;
		}

        if (physicalCard)
		    physicalCard.transform.localScale = Vector3.one * selectedCardSize;

		// set the selection indexes
		oldIndex = index;
		handIndex = index;
		
	}

    // this bad boy can fit so much <<USED FOR INPUT AND OTHER CHECKS>> in it.
    private void Update()
	{
        // if theres no cards anywhere in the system, hide this UI
		if (!visible && deckBuild.Count > 0)
		{
			visible = true;
			cardUI.gameObject.MoveTo(openUI.position, 0.5f, 0f);
		}
		else if (visible && deckBuild.Count == 0)
		{
			visible = false;
			cardUI.gameObject.MoveTo(closeUI.position, 0.5f, 0f);
		}

        // check inputs for changing the selected card
		if (!selectLock && inputAllow && handTransform.childCount > 0)
		{
			if (Input.GetButtonDown("Select_Right"))
			{
                 //SWITCH CARD SOUND
                UISound.SwitchCard();
                handIndex = (handIndex + 1) % hand.Count;
					SetSelection(handIndex);
			}

			if (Input.GetButtonDown("Select_Left"))
				{
                //SWITCH CARD SOUND
                UISound.SwitchCard();
                handIndex = handIndex - 1 < 0 ? hand.Count - 1 : handIndex - 1;
					SetSelection(handIndex);
			}

		}

        UpdateText();

        // STOP HERE IF INPUT NOT ALLOWED
        if (castLock || !inputAllow)
		{
			castPressed = true;
			return;
		}		

        // checks for casting requirements
		if (Input.GetAxis("Cast") == 1 && !castPressed)
		{
			if (cardPrefab != null && physicalCard != null)
			{
				if (physicalCard.GetComponent<CardProperties>().playable)
				{
					SpendMana(physicalCard.GetComponent<CardProperties>().info.manaCost);
					StartCoroutine(PlayCard(physicalCard, cardPrefab));
					castPressed = true;

					if (!manaRecharging)
						StartCoroutine(RechargeMana());
				}
			}
		}		
		else if (Input.GetAxis("Cast") != 1)
			castPressed = false;

		if (deckBuild.Count == 0)
			return;

		if (Input.GetButtonDown("Mulligan"))
		{
			mulliganPressed = true;
			player.ChangeSpeed(true);
		}

		if (Input.GetButtonUp("Mulligan"))
		{
			player.ChangeSpeed(false);
			mulliganPressed = false;
			pressTime = 0;
			refillMeter.fillAmount = 0;
		}

		if (mulliganPressed)
		{
			pressTime += Time.deltaTime;
			refillMeter.fillAmount = pressTime / mulliganHoldLength;
			if (pressTime >= mulliganHoldLength)
			{
				player.ChangeSpeed(false);
				mulliganPressed = false;
				pressTime = 0;
				refillMeter.fillAmount = 0;

				StartCoroutine(Mulligan());
			}
		}

        if (!manaRecharging)
        {
            if (mana < maxMana)
                RechargeMana();
        }
	}

    // this bad boy can fit so much <<PLAY THE CURRENTLY SELECTED CARD>> in it.
    public IEnumerator PlayCard(GameObject liveCard, GameObject card)
	{
        // don't let this do anything if we aren't allowed/can't cast anything
		if (castLock || hand.Count == 0)
			yield break;

        // hide the reticle
		reticle.gameObject.SetActive(false);

        foreach (Transform child in reticle)
            child.gameObject.SetActive(false);

        // lock card actions & movement actions
        castLock = true;
		selectLock = true;
		player.inputLock = true;

        
		CardProperties temp = liveCard.GetComponent<CardProperties>();

        // activate play effects, wait til finished
		StartCoroutine(temp.ActivateCondition(Condition.Play));
		while (!temp.completed)
			yield return null;

        // unlock movement actions
		player.inputLock = false;

        // send the card to discard pile, wait until its there
		yield return StartCoroutine(SendToDiscard(handIndex, liveCard));
		while (liveCard)
			yield return null;

		// draw a new card.
		yield return DrawCard(handIndex);

        // turn on all reticles again
		foreach (Transform child in reticle)
			child.gameObject.SetActive(true);

        reticle.gameObject.SetActive(true);

        // unlock card actions
        castLock = false;
		selectLock = false;
	}

    // this bad boy can fit so much <<REMOVE SPECIFIED AMOUNT OF MANA>> in it.
    void SpendMana(int cost)
	{
		mana -= cost;
		int orbIndex = 2;
		for(int i = 0; i < cost; ++i)
		{
			if (!manaOrbs[orbIndex].enabled)
			{
				i--;
				orbIndex--;
			}
			else
			{
				manaOrbs[orbIndex].enabled = false;
				manaOrbBlinks[orbIndex].Play("UnBlink");
				orbIndex--;
			}

			if (orbIndex < 0)
				break;
		}

		for (int index = 0; index < handTransform.childCount; ++index)
			CheckManaExpenses(handTransform.GetChild(index).GetComponent<CardProperties>());
	}

    // this bad boy can fit so much <<ADD SPECIFIED AMOUNT OF MANA>> in it.
    public void gainMana(int gain)
	{
		mana += gain;
		int orbIndex = 0;
		for (int i = 0; i < gain; ++i)
		{
			if (manaOrbs[orbIndex].enabled)
			{
				i--;
				orbIndex++;
			}
			else
			{
				manaOrbs[orbIndex].enabled = true;
				manaOrbBlinks[orbIndex].transform.Rotate(0, 0, Random.Range(0f, 360f));
				manaOrbBlinks[orbIndex].Play("Blink");
				orbIndex++;
			}

			if (orbIndex > 2)
				break;
		}

		for (int index = 0; index < handTransform.childCount; ++index)
			CheckManaExpenses(handTransform.GetChild(index).GetComponent<CardProperties>());

		if (handTransform.childCount > 0 && hand.Count > 0)
			SetSelection(handIndex);
	}

    // this bad boy can fit so much <<CHANGE CARD'S PLAYABILITY BASED ON AMOUNT OF MANA>> in it.
    public void CheckManaExpenses(CardProperties temp)
	{
		CardInfo tempInfo = temp.info;
		if (tempInfo.manaCost > mana)
			temp.SetPlayable(false);
		else
			temp.SetPlayable(true);
	}

    // this bad boy can fit so much <<UPDATE CARDPILE NUMBERS>> in it.
    void UpdateText()
	{
		deckText.text = liveDeck.Count.ToString();
		discardText.text = discardPile.Count.ToString();
	}

    // this bad boy can fit so much <<PUBLIC METHOD FOR STOPPING ALL CARD ACTION INPUT>> in it.
    public void ToggleInput(bool toggle)
	{
		inputAllow = toggle;
	}

    // this bad boy can fit so much <<COROUTINE FOR MANAGING MANA RECHARGE>> in it.
    IEnumerator RechargeMana()
	{
		// flip this boolean. Prevents this coroutine from being reset after dashing while the mana is recharging.
		manaRecharging = true;

		// wait the alloted time before adding mana.
		float timer = 0;
		while (timer < manaRechargeRate)
		{
			if (mana == maxMana)
			{
				manaRecharging = false;
				yield break;
			}			

			manaPercent = timer / manaRechargeRate;

			timer += Time.deltaTime;
			yield return 0;
		}
		gainMana(1);
        UISound.ManaGain();

		// if there is still more mana to recover, start a new coroutine instance.
		if (mana < maxMana)
			StartCoroutine(RechargeMana());
		// otherwise flip the boolean that keeps this from rolling.
		else
			manaRecharging = false;
	}

    // this bad boy can fit so much <<FUNCTION FOR ASSEMBLING A DROPDOWN OF CARDS IN THE EDITOR>> in it.
    public IList<ValueDropdownItem<int>> GetCardList()
	{
		ValueDropdownList<int> list = new ValueDropdownList<int>();

		for (int i = 0; i < availCards.Count; ++i)
			list.Add(availCards[i].name, i);

		return list;
	}

    public void DrawPileScaleUpdate(float scale)
    {
        deckTransform.localScale = Vector3.one * scale;
    }

    public void DiscardPileScaleUpdate(float scale)
    {
        discardTransform.localScale = Vector3.one * scale;
    }
}
