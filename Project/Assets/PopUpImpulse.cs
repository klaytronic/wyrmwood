﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PopUpImpulse : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField]
    private float forceAmount, angle;


    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponentInChildren<Rigidbody2D>();
        rb.AddForce(RollAngle() * forceAmount, ForceMode2D.Impulse);
    }

    private Vector2 RollAngle()
    {
        float range = Random.Range(-angle/2, angle/2);
        Vector2 direction = (Vector2)(Quaternion.Euler(0, 0, range ) * Vector2.up);
        return direction;
    }
}
